# worldsailing middle  

### Description
A midware for WS microservices.
Includes:

Template provider
   * PHP 
   * Source: src 
   * Entry point: web/index.php
   
API gateway
   * PHP
   * Source: src
   * Entry point: web/sajax.php   
   
Front-end widgets
   * Javascript (ReactJs)
   * Source: web/webapp 
 
### Browser compatibility
   * IE9
   * IE10
   * IE11
   * Firefox
   * Safari
   * Opera
   * Chrome
   * Edge

### Pre-processors
  * Babel
  * Sass
  
### Compiler
  * Grunt
  
### Installing  

```
$ copmoser install
$ npm install
$ bower install
$ grunt
```
  
### Grunt setting to build production
See option "devMode" in package.json. If this option is false then css and js files are minimized, otherwise not. Default option is true.
```
{
    "name": "ws-api-gateway",
    "descrition": "World Sailing API Gateway",
    "version": "0.0.1",
    "devMode": false,
```
 
### License

All rights reserved
