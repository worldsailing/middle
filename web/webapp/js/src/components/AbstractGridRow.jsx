'use strict';
class AbstractGridRow extends React.Component {

    constructor(props) {
        super(props);
        this.click = this.click.bind(this);
    }

    click() {
        this.props.onClick(this);
    }

    render() {
        return (

            <tr>
                <td>{this.props.item.id}</td>
                <td>{this.props.item.description}</td>
                <td><Button onClick={this.props.onClick} text="Edit" data-id={this.props.item.description} /></td>
            </tr>

        );
    }
}
