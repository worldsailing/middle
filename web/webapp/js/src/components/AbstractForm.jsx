'use strict';
class AbstractForm extends React.Component {

    constructor(props) {
        super(props);

        /* it must be set up in ChildClassForm.componentDidMount() */
        this.id = 0;
        this.entity = null;
        this.model = null;
        this.cancelUrl = '';
        this.dataUrl = '';
        this.submitUrl = '';
        this.returnUrl = '';

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentDidMount() {
        this.id = (this.props.id && this.props.id != '' && this.props.id != 0) ? this.props.id : 0;
        this.model = null;
        if (this.id != 0) {
            this.loadData();
        } else {
            this.setData();
        }
    }


    loadData() {

        var form = this;

        app.Http.get(this.dataUrl + '&id=' + this.id, function (response) {
                form.model = response.data;
                form.setData();
            },
            function (response) {
                app.Http.handleXhrError(response);
            });
    }


    setData() {
        var state = this.state;
        if (this.model !== null) {
            for (var i = 0; i < Object.keys(this.entity.prototype._fieldModel()).length; i++) {
                state[Object.keys(this.entity.prototype._fieldModel())[i]] = app.Util.getValue(this.model, Object.keys(this.entity.prototype._fieldModel())[i], "");
            }
        } else {
            for (var i = 0; i < Object.keys(this.entity.prototype._fieldModel()).length; i++) {
                state[Object.keys(this.entity.prototype._fieldModel())[i]] = this.entity.prototype._getDefault(Object.keys(this.entity.prototype._fieldModel())[i]);
            }
        }

        this.setState(state);
    }



    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});
    }


    handleSubmit() {
        var data = this.state;
        var form = this;
        app.Http.put(this.submitUrl + '&id=' + data.id, data, function (response) {
                location.href = form.returnUrl;
                return false;
            },
            function (response) {
                app.Http.handleXhrError(response);
            });
        return false;
    }

    handleCancel() {
        location.href = this.cancelUrl;
    }

    render() {

        return (
            <div>
                <fieldset>
                    <input id="id" name="id" type="hidden"  value={this.state.id} onChange={this.handleChange}/>

                    <div className="form-group">
                        <label htmlFor="description">Description</label>
                        <input type="text" className="form-control" id="BiogIsafId" name="description" placeholder="" value={this.state.description} onChange={this.handleChange}/>
                    </div>
                </fieldset>
                <div>
                    <Button onClick={this.handleSubmit} text="Save" /> <Button onClick={this.handleCancel} text="Cancel" />
                </div>
            </div>
        )
    }
}
