'use strict';
class ClickableColumnHeader extends  Link {

    render() {
        return (

            <th className={((this.props.class) ? this.props.class : app.Style.clickableColumnHeaderClass())}
                onClick={this.click} >{this.props.text}</th>

        );
    }
}
