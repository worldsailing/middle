'use strict';
class AbstractGrid extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: []
        };

        this.rootUrl = '/ajax.php';
        this.count = 0;
        this.data = false;
        this.offset = 0;
        this.limit = 25;
        this.page = 1;
        this.fetchAll = props.fetchAll;
        this.order = (props.order && typeof props.order != 'undefined') ? props.order : {"name" : '', "direction" : ''};

        this.handleOrder = this.handleOrder.bind(this);
        this.handleOffset = this.handleOffset.bind(this);
        this.handlePage = this.handlePage.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }


    componentDidMount() {
        this.loadData();
    }


    loadData() {

        if ((this.fetchAll === true) && (this.data !== false)) {
            this.setData();
        } else {
            var grid = this;
            var url = grid.url + '&page=' + ((grid.props.fetchAll === true) ? '-1' : grid.page);
            if (grid.order.name != '') {
                url = url + '&' + app.Util.serializeJson(grid.order, 'order');
            }
            app.Http.get(url, function (response) {
                    grid.data = response.data.result;
                    grid.count = response.data.records_in_scope;
                    grid.setData();
                },
                function (response) {
                    app.Http.handleXhrError(response);
                });
        }
    }

    createGridRow(item) {
        return '';
    }


    setData() {

        var data = this.data;
        var rows = [];

        if (this.fetchAll ) {

            this.count = data.length;

            if (this.order.name) {
                data = app.Util.orderBy(this.order.name, this.order.direction, data);
            }

            this.offset = this.limit * (this.page - 1);
            if (this.offset > 0) {
                if ((this.count - (this.offset + this.limit)) < 0) {
                    if (this.count < this.offset) {
                        this.page--;
                        this.setData();
                    }
                }
            } else {
                this.offset = 0;
                this.page = 1;
            }
            data = data.slice(this.offset, (this.offset + this.limit));
        }

        var grid = this;
        data.forEach(function (item) {
            rows.push(grid.createGridRow(item));
        });

        this.setState({"rows": rows});
    }


    toggleOrder(e, current) {
        var order = 'asc';
        if (e.props.name == current.name) {
            if (current.direction == 'asc') {
                order = 'desc';
            } else if (current.direction == 'desc') {
                order = 'asc';
            } else {
                order = 'asc';
            }
        }

        return order;
    }


    handleOrder(e) {
        var order = this.order;
        order.direction = this.toggleOrder(e, order);
        order.name = e.props.name;
        this.offset = 0;
        this.page = 1;
        this.order = order;
        this.loadData();
    }


    handleOffset(e) {
        //var page = this.page;
        this.page = parseInt(this.page) + parseInt(e.props['data-page']);
        this.page = (this.page >= 1) ? this.page : 1;
        this.loadData();
    }

    handlePage(e) {
        //var currentPage = this.state.page;
        this.page = e.props['data-page'];

        if (this.page == -1) {
            //last page
            this.page = Math.ceil(this.count / this.limit);

        }
        this.loadData();
    }


    handleEdit(e) {
        console.log(e);
    }

    render() {

        return (
            <div>
                <table className={app.Style.defaultTableClass()}>
                    <thead>
                    <tr>
                        <ClickableColumnHeader text={((this.order.name == 'id') ? <OrderIcon text="Id" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Id')}
                                               name="id"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.order.name == 'description') ?  <OrderIcon text="Description" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Description')}
                                               name="description"
                                               onClick={this.handleOrder} />
                    </tr>
                    </thead>
                    <tbody>{this.state.rows}</tbody>
                </table>
                <span>{this.page} / {Math.ceil(this.count/this.limit)} page</span><br />
                <PaginationButton text="<< First" onClick={this.handlePage} data-page="1"/> <PaginationButton text="< Prev" onClick={this.handleOffset} data-page="-1"/> <PaginationButton text="Next >" onClick={this.handleOffset} data-page="1"/> <PaginationButton text="Last >>" onClick={this.handlePage} data-page="-1"/>
            </div>
        );
    }
}
