'use strict';
class PaginationButton extends Button {

    render() {

        return (

            <button className={((this.props.class) ? this.props.class : app.Style.defaultButtonClass())}
                    type="button"
                    data-page={this.props['data-page']}
                    onClick={this.click}>{this.props.text}</button>

        );
    }
}
