'use strict';
app.Http = {

    get: function (url, success, error) {

        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.onload = function () {

            if (app.Util.checkResultObject(xhr.response, true)) {
                var response = app.Util.parseResult(xhr.response);
                if (response.success && response.success === true) {
                    if (app.Http.checkResponseCode(response, [200, 201, 202, 203, 204, 205, 206])) {
                        success(response);
                    } else {
                        app.Util.log(xhr.toString());
                        error(response);
                    }
                } else {
                    app.Util.log(xhr.toString());
                    error(response);
                }
            } else {
                app.Util.log(xhr.toString());
                error(null);
            }

        };

        xhr.onerror = function () {
            app.Util.log(xhr.toString());
            error(null);
        };

        xhr.send();
    },

    put: function (url, data, success, error) {

        var xhr = new XMLHttpRequest();
        xhr.open("PUT", url);
        xhr.onload = function () {
            if (app.Util.checkResultObject(xhr.response, true)) {
                var response = app.Util.parseResult(xhr.response);
                if (response.success && response.success === true) {
                    if (app.Http.checkResponseCode(response, [200, 201, 202])) {
                        success(response);
                    } else {
                        app.Util.log(xhr.toString());
                        error(response);
                    }
                } else {
                    app.Util.log(xhr.toString());
                    error(response);
                }
            } else {
                app.Util.log(xhr.toString());
                error(null);
            }

        };

        xhr.onerror = function () {
            app.Util.log(xhr.toString());
            success(null);
        };

        xhr.send(app.Util.serializeJson(data));
    },


    post: function (url, data, success, error) {

        var xhr = new XMLHttpRequest();
        xhr.open("POST", url);
        xhr.onload = function () {

            if (app.Util.checkResultObject(xhr.response, true)) {
                var response = app.Util.parseResult(xhr.response);
                if (response.success && response.success === true) {
                    if (app.Http.checkResponseCode(response, [200, 201, 202])) {
                        success(response);
                    } else {
                        app.Util.log(xhr.toString());
                        error(response);
                    }
                } else {
                    app.Util.log(xhr.toString());
                    error(response);
                }
            } else {
                app.Util.log(xhr.toString());
                error(null);
            }

        };

        xhr.onerror = function () {
            app.Util.log(xhr.toString());
            success(null);
        };

        xhr.send(app.Util.serializeJson(data));
    },

    delete: function (url, data, success, error) {

        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", url);
        xhr.onload = function () {

            if (app.Util.checkResultObject(xhr.response, true)) {
                var response = app.Util.parseResult(xhr.response);
                if (response.success && response.success === true) {
                    if (app.Http.checkResponseCode(response, [200, 204])) {
                        success(response);
                    } else {
                        app.Util.log(xhr.toString());
                        error(response);
                    }
                } else {
                    app.Util.log(xhr.toString());
                    error(response);
                }
            } else {
                app.Util.log(xhr.toString());
                error(null);
            }

        };

        xhr.onerror = function () {
            app.Util.log(xhr.toString());
            success(null);
        };

        xhr.send(app.Util.serializeJson(data));
    },


    checkResponseCode: function (response, acceptedCodes) {
        if (acceptedCodes == null || typeof acceptedCodes == 'undefined') {
            acceptedCodes = [200, 201, 202, 203, 204, 205, 206];
        }

        if (acceptedCodes.indexOf(response.code) >= 0) {
            return true;
        } else {
            return false;
        }
    },


    handleXhrError: function (response) {
        if (response.data && typeof response.data != 'undefined' && response.data.error && typeof response.data.error !== 'undefined') {
            alert(response.data.error);
        } else {
            alert((response.message && typeof response.message != 'undefined' && response.message != '') ? response.message : 'An error occurred');
        }
        return false;
    }

}
