'use strict';
app.Util = {

    uuid: function () {
        var i, random;
        var uuid = '';

        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += '-';
            }
            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
                .toString(16);
        }

        return uuid;
    },

    log: function(something){
        console.log(something);
    },

    getValue: function(obj, key, def) {
        var d = (typeof def != 'undefined') ? def : null;
        return (this.getObjectSize(obj) && obj[key] && (obj[key] != 'undefined')) ? obj[key] : d;
    },

    getObjectSize: function(obj){
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    },

    isEmpty: function(obj) {

        if (obj == null) return true;

        if (obj.length > 0)    return false;  //string or array
        if (obj.length === 0)  return true;   //string or array
        if (typeof(obj) === 'object'){  // declared object, but there is no keys
            var count = 0 ;
            for (var i in obj) {
                if (obj.hasOwnProperty(i)) {
                    count++;
                }
            }
            if (count > 0){
                return false;
            }
        }
        return true;
    },

    checkResultObject: function(res, nullIsTrue){
        if (!(typeof res =='object')){
            if (this.isEmpty(res)){
                return (nullIsTrue === true) ? true : false
            } else {
                return true;
            }
        } else {
            return true;
        }
    },

    parseResult: function(json){
        try {
            if (!(typeof json === 'object')){
                return jQuery.parseJSON(json);
            } else
            {
                return json;
            }
        }catch(e)
        {
            return json;
        }
    },

    getJson: function(name, done) {
        var fileName = 'data/' + name ;

        var xhr = new XMLHttpRequest();
        xhr.open("GET", fileName);
        xhr.onload = function () {
            done(((app.Util.checkResultObject(xhr.response, true)) ? app.Util.parseResult(xhr.response) : null));
        };
        xhr.onerror = function () {
            app.Util.log(xhr.toString());
            done(null);
        };
        xhr.send();
    },

    orderBy: function(field_name, direction, data) {
        if (direction == 'asc') {
            data.sort(function (a, b) {
                if (a[field_name] < b[field_name]) return -1;
                if (a[field_name] > b[field_name]) return 1;
                return 0;

            });
        } else if (direction == 'desc') {
            data.sort(function(a, b){
                if(a[field_name] < b[field_name]) return 1;
                if(a[field_name] > b[field_name]) return -1;
                return 0;

            })
        }
        return data;
    },

    serializeJson: function(obj, prefix) {
        var str = [], p;
        for(p in obj) {
            if (obj.hasOwnProperty(p)) {
                var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
                str.push((v !== null && typeof v === "object") ?
                    serialize(v, k) :
                encodeURIComponent(k) + "=" + encodeURIComponent(v));
            }
        }
        return str.join("&");
    },

    getColumnHeaderText: function(text, direction) {
        return text + ' ' + direction
    }

}
