'use strict';
app.ExampleEntity = function (props) {
    this.BiogMembId = app.Util.getValue(props, 'BiogMembId', this._getDefault('BiogMembId'));
    this.BiogIsafId = app.Util.getValue(props, 'BiogIsafId', this._getDefault('BiogIsafId'));
    this.BiogFirstName = app.Util.getValue(props, 'BiogFirstName', this._getDefault('BiogFirstName'));
    this.BiogSurname = app.Util.getValue(props, 'BiogSurname', this._getDefault('BiogSurname'));
    this.BiogEmail = app.Util.getValue(props, 'BiogEmail', this._getDefault('BiogEmail'));
    this.CreatedAt = app.Util.getValue(props, 'CreatedAt', this._getDefault('CreatedAt'));
    this.UpdatedAt = app.Util.getValue(props, 'UpdatedAt', this._getDefault('UpdatedAt'));
};

app.ExampleEntity.prototype._fieldModel = function () {
    return {
        BiogMembId: '',
        BiogIsafId: '',
        BiogFirstName: '',
        BiogSurname: '',
        BiogEmail: '',
        CreatedAt: '',
        UpdatedAt: ''
    }
};

app.ExampleEntity.prototype._getDefault = function (key){
    return app.Util.getValue(app.ExampleEntity.prototype._fieldModel(), key, '');
};
