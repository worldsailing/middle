'use strict';
app.Bootstrap = app.Bootstrap  || {}

app.Bootstrap.showFormWidget =  function(id, readOnly, csrfName, csrfValue){

    readOnly = (readOnly === true) || false;
    ReactDOM.render(
        <ExampleFormWidget id={id} readOnly={readOnly} csrfName={csrfName} csrfValue={csrfValue}/>, document.getElementById('widget-container')
    );

}
