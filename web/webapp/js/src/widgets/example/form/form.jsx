'use strict';
class ExampleFormWidget extends AbstractForm {

    constructor(props) {
        super(props);

        this.state = {
            [this.props.csrfName] : this.props.csrfValue,
            id : this.props.id
        };

        for (var i = 0; i < Object.keys(app.ExampleEntity.prototype._fieldModel()).length; i++) {
            this.state[Object.keys(app.ExampleEntity.prototype._fieldModel())[i]] = app.ExampleEntity.prototype._getDefault(Object.keys(app.ExampleEntity.prototype._fieldModel())[i]);
        }
        AbstractForm.prototype.constructor.call(this);
    }

    componentDidMount() {
        this.cancelUrl = '/index.php?ws_layout_id=example.list';
        this.dataUrl = '/ajax.php?ws_request_id=example.edit';
        this.submitUrl = '/ajax.php?ws_request_id=example.edit';
        this.returnUrl = 'http://middle.local/index.php?ws_layout_id=example.list';
        this.entity = app.ExampleEntity;
        AbstractForm.prototype.componentDidMount.call(this);
    }


    render() {
        return (
            <div className="col-lg-8">
                <fieldset>
                    <input id="id" name="id" type="hidden"  value={this.state.id} onChange={this.handleChange}/>
                    <input id="BiogMembId" name="BiogMembId" type="hidden"  value={this.state.BiogMembId} onChange={this.handleChange}/>

                    <div className="form-group">
                        <label htmlFor="BiogIsafId">ISAF ID</label>
                        <input type="text" className="form-control" id="BiogIsafId" name="BiogIsafId" placeholder="" value={this.state.BiogIsafId} onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="BiogFirstName">First Name</label>
                        <input type="text" className="form-control" id="BiogFirstName" name="BiogFirstName" placeholder="" value={this.state.BiogFirstName} onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="BiogSurname">Surname</label>
                        <input type="text" className="form-control" id="BiogSurname" name="BiogSurname" placeholder="" value={this.state.BiogSurname} onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="BiogEmail">Email</label>
                        <input type="text" className="form-control" id="BiogEmail" name="BiogEmail" placeholder="" value={this.state.BiogEmail} onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="CreatedAt">Created At</label>
                        <input type="text" className="form-control" id="CreatedAt" name="CreatedAt" placeholder="" disabled="disabled"  value={this.state.CreatedAt} onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="UpdatedAt">Updated At</label>
                        <input type="text" className="form-control" id="UpdatedAt" name="UpdatedAt" placeholder="" disabled="disabled" value={this.state.UpdatedAt} onChange={this.handleChange} />
                    </div>
                    <div className="form-group">
                        <Button onClick={this.handleSubmit} text="Save" /> <Button onClick={this.handleCancel} text="Cancel" />
                    </div>
                </fieldset>
            </div>
        )
    }
}
