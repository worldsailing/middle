'use strict';
app.Bootstrap = app.Bootstrap  || {}

app.Bootstrap.showGridWidget =  function(fetchAll, defaultOrder){

    var order = (defaultOrder && typeof defaultOrder != 'undefined') ? defaultOrder : {'name': "", "direction": ""}
    ReactDOM.render(
        <ExampleGridWidget fetchAll={fetchAll} order={order} />, document.getElementById('widget-container')
    );

}
