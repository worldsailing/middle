'use strict';
class ExampleGridRow extends AbstractGridRow {

    render() {
        return (

            <tr>
                <td>{this.props.item.BiogMembId}</td>
                <td>{this.props.item.BiogIsafId}</td>
                <td>{this.props.item.BiogFirstName}</td>
                <td>{this.props.item.BiogSurname}</td>
                <td>{this.props.item.BiogEmail}</td>
                <td>{this.props.item.CreatedAt}</td>
                <td>{this.props.item.UpdatedAt}</td>
                <td><Button onClick={this.props.onClick} text="Edit" data-id={this.props.item.BiogMembId} /></td>
            </tr>

        );
    }
}
