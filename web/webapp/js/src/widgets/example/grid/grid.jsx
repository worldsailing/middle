'use strict';
class ExampleGridWidget extends AbstractGrid {

    componentDidMount() {
        this.url = this.rootUrl + '?ws_request_id=example.list';
        this.offset = 0;
        this.limit = 25;
        this.loadData();
    }

    handleEdit(e) {
        location.href = '/index.php?ws_layout_id=example.edit&id=' + e.props['data-id'];
    }

    createGridRow(item) {
        return <ExampleGridRow item={item} key={app.Util.uuid()} onClick={this.handleEdit}/>;
    }

    render() {

        return (
            <div>
                <table className={app.Style.defaultTableClass()}>
                    <thead>
                    <tr>
                        <ClickableColumnHeader text={((this.order.name == 'BiogMembId') ? <OrderIcon text="Id" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Id')}
                                               name="BiogMembId"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.order.name == 'BiogIsafId') ?  <OrderIcon text="Sailor Id" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Sailor Id')}
                                               name="BiogIsafId"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={ ((this.order.name == 'BiogFirstName') ? <OrderIcon text="First Name" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'First Name')}
                                               name="BiogFirstName"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.order.name == 'BiogSurname') ?  <OrderIcon text="Surname" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Surname')}
                                               name="BiogSurname"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.order.name == 'BiogEmail') ?  <OrderIcon text="Email" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Email')}
                                               name="BiogEmail"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.order.name == 'CreatedAt') ? <OrderIcon text="Created At" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Created At')}
                                               name="CreatedAt"
                                               onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.order.name == 'UpdatedAt') ? <OrderIcon text="Updated At" class={app.Style.defaultOrderDirectionIcon(this.order.direction)}></OrderIcon> : 'Updated At')}
                                               name="UpdatedAt"
                                               onClick={this.handleOrder} />
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>{this.state.rows}</tbody>
                </table>
                <span>{this.page} / {Math.ceil(this.count/this.limit)} page</span><br />
                <PaginationButton text="<< First" onClick={this.handlePage} data-page="1"/> <PaginationButton text="< Prev" onClick={this.handleOffset} data-page="-1"/> <PaginationButton text="Next >" onClick={this.handleOffset} data-page="1"/> <PaginationButton text="Last >>" onClick={this.handlePage} data-page="-1"/>
            </div>
        );
    }
}
