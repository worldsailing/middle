<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

require_once __DIR__ . '/../src/WsApp.php';

$params = [];

// Kick off API gateway
WsApp::getInstance()->request($_REQUEST['ws_request_id'], $params);
