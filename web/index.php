<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

require_once __DIR__ . '/../src/WsApp.php';

$params = [];

// Kick off template provider
WsApp::getInstance()->layout(isset($_REQUEST['ws_layout_id']) ? $_REQUEST['ws_layout_id'] : 'example.list', $params);
