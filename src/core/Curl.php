<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
/**
 * Class Curl
 * @package Core
 */
/**
 * Class Curl
 * @package Core
 */
Class Curl extends AbstractRestProtocol {

    /**
     * @var
     */
    private $url;
    /**
     * @var string
     */
    private $method = 'GET';
    /**
     * @var array
     */
    private $postData = array();
    /**
     * @var int
     */
    private $dataLength = 0;
    /**
     * @var array
     */
    private $headers = array();
    /**
     * @var string
     */
    private $dataType = 'array'; //array, json

    /**
     * Curl constructor.
     */
    public function __construct()
    {

    }


    /**
     * @return $this
     */
    public function init()
    {
        $this->url = '';
        $this->method = 'GET';
        $this->postData = array();
        $this->dataType = 'array';
        $this->dataLength = 0;
        $this->headers = array();
        return $this;
    }


    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }


    /**
     * @param $data
     * @param string $type
     * @return $this
     */
    public function setData($data, $type = 'array')
    {
        $this->dataType = $type;
        $this->postData = $data;
        if ($this->dataType == 'json') {
            $this->dataLength = strlen(json_encode($this->postData));
        } else {
            $this->dataLength = strlen(http_build_query($this->postData));
        }

        return $this;
    }


    /**
     * @param array $headers
     * @return $this
     */
    public function setHeader($headers = array())
    {
        $this->headers = $headers;
        return $this;
    }


    /**
     * @param string $rootUrl
     * @param string $urlParameterString
     * @return $this
     */
    public function setUrl($rootUrl, $urlParameterString = '')
    {
        if ($urlParameterString != '') {
            $urlParameterString = str_replace(' ', '%20', $urlParameterString);
            $this->dataLength = strlen((string) $urlParameterString);
        } else {
            $this->dataLength = 0;
        }

        $this->url = $rootUrl . $urlParameterString;
        return $this;
    }


    /**
     * @param int $ssl
     * @param int $sendHeader
     * @param string $username
     * @param string $password
     * @return RestProtocolResult
     */
    public function run($ssl = 0, $sendHeader = 0, $username = '', $password = '')
    {

        $result = new RestProtocolResult();

        try {

            set_time_limit(60);

            $curlSession = curl_init();

            $result->url($this->url);

            curl_setopt($curlSession, CURLOPT_URL, $this->url);
            curl_setopt($curlSession, CURLOPT_SSL_VERIFYPEER, ($ssl === 1) ? 1 : 0);
            curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlSession, CURLOPT_FOLLOWLOCATION, true);
            if ($username != '') {
                curl_setopt($curlSession, CURLOPT_USERPWD, $username . ":" . $password);
            }
            // if this is post request, set post data
            if ($this->method == 'POST') {
                curl_setopt($curlSession, CURLOPT_POST, 1);
                curl_setopt($curlSession, CURLOPT_POSTFIELDS, (($this->dataType == 'json' ) ? json_encode($this->postData) : http_build_query($this->postData)) );
            } elseif ($this->method == 'PUT') {
                curl_setopt($curlSession, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curlSession, CURLOPT_POSTFIELDS, (($this->dataType == 'json' ) ? json_encode($this->postData) : http_build_query($this->postData)) );
            } elseif ($this->method == 'DELETE') {
                curl_setopt($curlSession, CURLOPT_CUSTOMREQUEST, "DELETE");
            }

            //if header is necessary
            if ($sendHeader === 1) {
                if ($this->method == 'POST') {
                    //post header contaions data length
                    $headers = array_merge($this->headers, array('Content-Type: application/json', 'Content-Length: ' . $this->dataLength));

                } else {

                    $headers = array_merge($this->headers, array('Content-Type: application/json'));
                }
                curl_setopt($curlSession, CURLOPT_HEADER, true);
                curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headers);
            }  else {
                curl_setopt($curlSession, CURLOPT_HEADER, false);
            }

            $response = curl_exec($curlSession);

            WsApp::getInstance()->log()->debug($this->url);
            WsApp::getInstance()->log()->debug($response);

            //any exception during initialization and sending. (if something happens within execute, curl will handle that exception, and you can find errormessage in curl_error() )
        } catch(WsException $e){

            $result->message($e->getMessage());
            $result->success(false);

            return $result;
        }

        //Check that a connection was made
        if (curl_error($curlSession)){
            $result->message('e_curl|' . curl_error($curlSession));
            $result->success(false);
        } else
        {
            //If there is no curl error, you get back the "rare" response text, including headers
            $result->setData($response); //Here is not any further check or decode. Just you and god knows what is the expected structure. Handle it outside of this helper.
            $result->headerSize(curl_getinfo($curlSession,CURLINFO_HEADER_SIZE));
            $result->code(curl_getinfo($curlSession, CURLINFO_HTTP_CODE));
        }

        curl_close ($curlSession);

        return $result;

    }

}
