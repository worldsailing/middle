<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use Core\Response;
use Twig_Loader_Filesystem;
use Twig_Environment;

/**
 * Class WsException
 * @package Core
 */
class WsException extends \Exception
{

    /**
     * @param string $message
     */
    public function __construct($message, $code = 400)
    {
        parent::__construct($message, $code);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . ": {$this->message}\n";
    }


    /**
     *
     */
    public function show()
    {
        if (WsApp::getInstance()->isVisual()){

            $message = $this->__toString();
            $code = $this->getCode();

            try {
                $templates = array(
                    'error/' . $code . '.html.twig',
                    'error/' . substr($code, 0, 2) . 'x.html.twig',
                    'error/' . substr($code, 0, 1) . 'xx.html.twig',
                    'error/default.html.twig',
                );
                $loader = new Twig_Loader_Filesystem(WS_PATH_ROOT . '/src/template/default');
                $twig = new Twig_Environment($loader, array(
                    'cache' => false,
                    'debug' => false
                ));
                echo $twig->resolveTemplate($templates)->render(array('code' => $code, 'message' => $message));
            } catch (\Exception $e) {
                echo $this->__toString();
            }
        } else {
            /*
            It should create a Response object,
             and put error message into that, then run the response.
            */
            $response = new Response(false, ['error' => $this->getMessage()], $this->getMessage(), $this->getCode());
            $response->dump();
        }
    }
}
