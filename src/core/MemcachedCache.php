<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use worldsailing\Helper\WsHelper;
use \Memcached;

/**
 * Class MemcachedCache
 * @package Core
 */
class MemcachedCache extends AbstractCache implements Cacheinterface
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var Memcached
     */
    private $handle;

    /**
     * @var bool
     */
    public $compress = false;

    /**
     * @var int
     */
    private $defaultExpiry = 0; // seconds

    /**
     * @return MemcachedCache
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * MemcachedCache constructor.
     * @throws WsException
     */
    public function __construct()
    {
        $options = WsApp::getInstance()->config()->get('cache', 'MemcachedCache.options');

        $this->compress = (isset($options['compressEnabled'])) ? ($options['compressEnabled'] === true) : false;

        if ( isset($options['defaultExpiry']) ) {
            $this->defaultExpiry = ((int)$options['defaultExpiry'] > 0 ) ? (int) $options['defaultExpiry'] : 0;
        }

        try {
            $this->handle = new Memcached();
            foreach ($options['servers'] as $server) {
                $this->handle->addServer($server['host'], $server['port']);
            }
        }catch(\Exception $e) {
            WsApp::getInstance()->log()->error(WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            throw new WsException($e->getMessage(), $e->getCode());
        }
    }


    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     */
    public function set($key,$value,$ttl = 0)
    {
        if( 0 === $ttl && $this->defaultExpiry ) {
            $ttl = $this->defaultExpiry;
        }
        try {
            $this->sanitize($key);
            $this->handle->set( $this->sanitize($key) , serialize( $value ) , $ttl );
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcached error at writing cache [MemcachedCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     * @param $key
     * @param int $ttl
     * @param null $default
     * @return mixed
     */
    public function get($key, $ttl = 0, $default = null)
    {
        try {
            $v = $this->handle->get( $this->sanitize($key) );

            if ( $v ) {
                return unserialize($v);
            }
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcached error at reading cache [MemcachedCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            return $default;
        }
    }

    /**
     * @param string $key
     */
    public function remove($key)
    {
        try {
            $this->handle->delete( $this->sanitize($key));
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcached error at deleting cache [MemcachedCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     *
     */
    public function clear()
    {
        try {
            $this->handle->flush();
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcached error at clearing cache [MemcachedCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        try {
            return ($this->handle->get( $this->sanitize($key) ) !== false) ? true : false;
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcached error at reading cache [MemcachedCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            return false;
        }
    }
}
