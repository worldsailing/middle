<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

/**
 * Class AbstractRestProtocol
 * @package Core
 */
Abstract Class AbstractRestProtocol
{

    /**
     * @var
     */
    protected static $instance;

    /**
     * AbstractRestProtocol constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static();
        }
        return self::$instance;
    }

    /**
     * @return mixed
     */
    abstract public function init();

    /**
     * @param string $method
     * @return $this
     */
    abstract public function setMethod($method);

    /**
     * @param $data
     * @param string $type
     * @return $this
     */
    abstract public function setData($data, $type = 'array');

    /**
     * @param array $headers
     * @return $this
     */
    abstract public function setHeader($headers = array());

    /**
     * @param string $rootUrl
     * @param string $urlParameterString
     * @return $this
     */
    abstract public function setUrl($rootUrl, $urlParameterString = '');

    /**
     * @param int $ssl
     * @param int $sendHeader
     * @param string $username
     * @param string $password
     * @return RestProtocolResult
     */
    abstract public function run($ssl = 0, $sendHeader = 0, $username = '', $password = '');

}
