<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use worldsailing\Helper\WsHelper;
/**
 * Class Response
 * @package Core
 */
class Response {

    /**
     * @var bool
     */
    public $success;

    /**
     * @var null
     */
    public $data;

    /**
     * @var string
     */
    public $message;

    /**
     * @var int
     */
    public $responseCode;

    /**
     * @var string
     */
    public $callback = '';

    /**
     * @var mixed
     */
    public $cached = null;


    /**
     * Response constructor.
     * @param bool $success
     * @param null $data
     * @param string $message
     * @param int $responseCode
     * @param bool $cached
     */
    public function __construct(
        $success = true,
        $data = null,
        $message = '',
        $responseCode = 200,
        $cached = null
    )
    {
        $this->success = ($success === true) ? true : false;
        $this->data = $data;
        $this->message = $message;
        $this->responseCode = $responseCode;
        $this->cached = $cached;
    }


    /**
     * @param $callback
     * @return $this
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
        return $this;
    }


    /**
     *
     */
    public function dump()
    {
        header('Content-Type: application/json');
        if ($this->message == '') {
            $this->message = WsHelper::getResponseMessageByCode($this->responseCode);
        }
        header(WsHelper::getResponseMessageByCode($this->responseCode, true));
        echo $this->json();
        exit;
    }

    /**
     * @return string
     */
    public function json()
    {
        return json_encode([
            'success' => $this->success,
            'data' => $this->data,
            'message' => $this->message,
            'callback' => $this->callback,
            'code' => $this->responseCode,
            'cached' => $this->cached
        ]);
    }

}
