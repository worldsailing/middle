<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Core\WsTwigExtension;

/**
 * Class Widget
 * @package Core
 */
class Widget {

    /**
     * @var string
     */
    private $theme;

    /**
     * @var bool
     */
    private $cache;

    /**
     * @var array|mixed
     */
    private $debug;

    /**
     * @var string
     */
    private $content = '';

    /**
     * Widget constructor.
     * @param string $theme
     * @param string $template
     * @param array $parameters
     */
    public function __construct($theme, $template, $parameters = [])
    {
        $template = $this->getTwigFileName($template);
        WsApp::getInstance()->log()->debug('Rendered template', [$template]);

        //Load twig behaviours
        $this->theme = $theme;
        $cacheEnabled = WsApp::getInstance()->config()->get('common', 'twig.cacheEnabled');
        if ($cacheEnabled === true) {
            $this->cache = WsApp::getInstance()->getDirPath(WsApp::getInstance()->config()->get('common', 'directories.pathCache') . '/twig');
        } else {
            $this->cache = false;
        }
        $this->debug = WsApp::getInstance()->config()->get('common', 'twig.debugEnabled');

        //Load twig environment
        $twig = $this->getTwigLoader();

        //Render template
        $this->content = $twig->render($template, $parameters);
    }


    /**
     * @param string $template
     * @return string
     */
    private function getTwigFileName($template)
    {
        $delimiter = WsApp::getInstance()->getDelimiter();
        return str_replace($delimiter, '/', $template) . '.html.twig';
    }


    /**
     * @return Twig_Environment
     */
    private function getTwigLoader()
    {
        $loader = new Twig_Loader_Filesystem(WS_PATH_ROOT . '/src/template/' . $this->theme);
        $loader->addPath(WS_PATH_ROOT . '/web/webapp', 'webapp');
        $twig = new Twig_Environment($loader, array(
            'cache' => $this->cache,
            'debug' => $this->debug
        ));
        $twig->addExtension(new WsTwigExtension());
        return $twig;
    }


    /**
     *
     */
    public function dump()
    {
        echo $this->content;
    }


}
