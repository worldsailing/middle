<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use worldsailing\Helper\WsHelper;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

/**
 * Class FileSystemCache
 * @package Core
 */
class FileSystemCache  extends AbstractCache implements CacheInterface
{

    /**
     * @var
     */
    protected static $instance;

    /**
     * @var int
     */
    private $maxFileNameLength = 255;

    /**
     * @var int
     */
    private $defaultExpiry = 0; // seconds

    /**
     * @var bool
     */
    private $truncateEnabled = false;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * FileSystemCache constructor.
     * @throws WsException
     */
    public function __construct()
    {

        $options = WsApp::getInstance()->config()->get('cache', 'FileSystemCache.options');

        if ( isset($options['$maxFileNameLength']) ) {
            $this->maxFileNameLength = (((int)$options['$maxFileNameLength'] > 0 ) && ((int)$options['$maxFileNameLength'] <= 255)) ? (int) $options['$maxFileNameLength'] : 255;
        }

        if ( isset($options['defaultExpiry']) ) {
            $this->defaultExpiry = ((int)$options['defaultExpiry'] > 0 ) ? (int) $options['defaultExpiry'] : 0;
        }

        if ( isset($options['truncateEnabled']) ) {
            $this->truncateEnabled = ($options['truncateEnabled'] === true ) ? true : false;
        }

        if (isset($options['cacheDir']) && strlen($options['cacheDir']) > 0) {
            $this->cacheDir = WsApp::getInstance()->getDirPath($options['cacheDir']) ;
        } else {
            WsApp::getInstance()->log()->error('Undefined cache directory', ['Class' => 'FileSystemCache']);
            throw new WsException('Undefined cache directory [FileSystemCache]');
        }

        try {
            if (!file_exists($this->cacheDir)) {
                mkdir($this->cacheDir, 0755, true);
            }
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Invalid cache directory [FileSystemCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            throw new WsException($e->getMessage() . ' [FileSystemCache]', $e->getCode());
        }
    }

    /**
     * @return FileSystemCache
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param $string
     * @return string
     */
    protected function sanitize($string) {
        $clean = parent::sanitize($string);
        if (strlen($clean) > $this->maxFileNameLength) {
            if ($this->truncateEnabled) {
                $clean = substr($clean, 0,  $this->maxFileNameLength);
            } else {
                $clean = ''; //We are not able to cache this request
            }
        }
        return trim($clean);
    }

    /**
     * @param $key
     * @return string
     */
    private function getCacheFilePath($key)
    {
        $filename = $this->sanitize($key);
        if ($filename <> '') {
            $filename =  $this->cacheDir . DIRECTORY_SEPARATOR . $filename;
        }
        return $filename;
    }

    /**
     * @param $file
     * @return string
     */
    private function decodeFile($file)
    {
        $content = file_get_contents($file);
        $content = unserialize($content);
        return $content;
    }

    /**
     * @param $file
     * @param $data
     * @return int
     */
    private function encodeFile($file, $data)
    {
        $content = serialize($data);
        return file_put_contents( $file, $content );
    }

    /**
     * @param $key
     * @param int $ttl
     * @param null $default
     * @return null|string
     */
    public function get($key, $ttl = 0, $default = null)
    {
        $path = $this->getCacheFilePath($key);

        if(($path == '') || (! file_exists($path)))
            return $default;

        // is expired ?
        if ($ttl > 0) {
            $expiry = $ttl;
        } else {
            $expiry = $this->defaultExpiry;
        }

        if( $expiry && (time() - filemtime($path)) > $expiry ) {
            try {
                unlink( $path ) ;
            } catch(\Exception $e) {
                WsApp::getInstance()->log()->warning('Unable to delete item from filesystem cache', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            }
            return null;
        }

        return $this->decodeFile($path);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return bool
     */
    public function set($key , $value, $ttl = 0)
    {
        $path = $this->getCacheFilePath($key);
        if ($path == '') {
            return false;
        } else {

        }
        return $this->encodeFile($path, $value) !== false;
    }


    /**
     * @param string $key
     */
    public function remove($key)
    {
        $path = $this->getCacheFilePath($key);
        if ($path != '' && file_exists($path) ) {
            try {
                unlink( $path ) ;
            } catch(\Exception $e) {
                WsApp::getInstance()->log()->warning('Unable to delete item from filesystem cache', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            }
        }
    }

    /**
     *
     */
    public function clear()
    {
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->cacheDir),
            RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($iterator as $path) {
            if( $path->isFile() ) {
                try {
                    unlink( $path->__toString() ) ;
                } catch(\Exception $e) {
                    WsApp::getInstance()->log()->warning('Unable to delete item from filesystem cache', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
                }
            }
        }
    }

    /**
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        $path = $this->getCacheFilePath($key);
        return ($path != '' && file_exists($path) ) ? true : false;
    }
}

