<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use Memcache;
use WsApp;
use worldsailing\Helper\WsHelper;

/**
 * Class MemcacheCache
 * @package Core
 */
class MemcacheCache extends AbstractCache implements CacheInterface
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var Memcache
     */
    private $handle;

    /**
     * @var bool
     */
    public $compress = false;

    /**
     * @var int
     */
    private $defaultExpiry = 0; // seconds

    /**
     * @return MemcacheCache
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * MemcacheCache constructor.
     * @throws WsException
     */
    public function __construct()
    {
        $options = WsApp::getInstance()->config()->get('cache', 'MemcacheCache.options');

        $this->compress = (isset($options['compressEnabled'])) ? ($options['compressEnabled'] === true) : false;

        if ( isset($options['defaultExpiry']) ) {
            $this->defaultExpiry = ((int)$options['defaultExpiry'] > 0 ) ? (int) $options['defaultExpiry'] : 0;
        }
        try {
            $this->handle = new Memcache();
            foreach ($options['servers'] as $server) {
                $this->handle->connect($server['host'], $server['port']);
            }
        }catch(\Exception $e) {
            WsApp::getInstance()->log()->error(WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            throw new WsException($e->getMessage(), $e->getCode());
        }
    }


    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     */
    public function set($key,$value,$ttl = 0)
    {
        if( 0 === $ttl && $this->defaultExpiry ) {
            $ttl = $this->defaultExpiry;
        }
        try {
            $this->handle->set( $this->sanitize($key) , serialize( $value ) , $this->compress , $ttl );
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcache error at writing cache [MemcacheCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     * @param $key
     * @param int $ttl
     * @param null $default
     * @return mixed
     */
    public function get($key, $ttl = 0, $default = null)
    {
        try {
            $v = $this->handle->get( $this->sanitize($key) );
            if ( $v ) {
                return unserialize($v);
            }
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcache error at reading cache [MemcacheCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            return $default;
        }
    }

    /**
     * @param string $key
     */
    public function remove($key)
    {
        try {
            $this->handle->delete( $this->sanitize($key));
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcache error at deleting cache [MemcacheCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     *
     */
    public function clear()
    {
        try {
            $this->handle->flush();
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcache error at clearing cache [MemcacheCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        try {
            return ($this->handle->get( $this->sanitize($key) ) !== false) ? true : false;
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Memcache error at reading cache [MemcacheCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            return false;
        }
    }

}

