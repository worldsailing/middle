<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
/**
 * Class SessionCache
 * @package Core
 */
class SessionCache extends AbstractCache implements CacheInterface
{

    /**
     * @var
     */
    protected static $instance;

    /**
     * @var string
     */
    private $namespace = 'cache';

    /**
     * SessionCache constructor.
     * @throws WsException
     */
    public function __construct()
    {
        if (!(session_status() == PHP_SESSION_ACTIVE)) {
            throw new WsException('Session does not exists in class [SessionCache]');
        };

        $options = WsApp::getInstance()->config()->get('cache', 'SessionCache.options');

        $this->namespace = (isset($options['namespace']) && strlen($options['namespace']) > 0) ? $options['namespace'] : $this->namespace;
        if ( ! isset($_SESSION[$this->namespace])){
            $_SESSION[$this->namespace] = [];
        }
    }

    /**
     * @return SessionCache
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param string $key
     * @param int $ttl
     * @param null $default
     * @return array|int|null
     */
    public function get($key, $ttl = 0, $default = null)
    {
        if ( isset($_SESSION[$this->namespace][$key]) ) {
            return $_SESSION[$this->namespace][ $key ];
        } else {
            return $default;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     */
    public function set($key, $value, $ttl = 0)
    {
        $_SESSION[$this->namespace][ $key ] = $value;
    }

    /**
     * @param string $key
     */
    public function remove($key)
    {
        unset( $_SESSION[$this->namespace][ $key ] );
    }

    /**
     *
     */
    public function clear()
    {
        $_SESSION[$this->namespace] = [];
    }

    /**
     * @param $key
     * @return bool
     */
    public function exists($key)
    {
        return isset($_SESSION[$this->namespace][$key]);
    }

}

