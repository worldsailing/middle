<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

/**
 * Class WsTwigExtension
 * @package Core
 */
class WsTwigExtension extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            //Debugging filters
            new \Twig_SimpleFilter('json_decode', 'json_decode'),
            new \Twig_SimpleFilter('var_dump', 'var_dump'),
            new \Twig_SimpleFilter('get_object_vars', 'get_object_vars'),
            new \Twig_SimpleFilter('array_keys', 'array_keys'),
            new \Twig_SimpleFilter('current', 'current'),

            //Formatter filters
            new \Twig_SimpleFilter('price', array($this, 'priceFilter'))
        );
    }


    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            // URL functions
            new \Twig_SimpleFunction('getRootUrl', function() {
                return WsApp::getInstance()->getRootUrl();
            }),
            new \Twig_SimpleFunction('getAbsoluteUrl', function($url = '') {
                return WsApp::getInstance()->getAbsoluteUrl($url);
            }),

            // Html manipulation functions
            new \Twig_SimpleFunction('getCsrfTokenId', function($id) {
                return  self::getCsrfTokenId($id);
            }),
            new \Twig_SimpleFunction('getCsrfTokenValue', function($csrfId) {
                return  self::getCsrfTokenValue($csrfId);
            }),
            new \Twig_SimpleFunction('getCsrfToken', function($id) {
                return  json_encode([self::getCsrfTokenId($id) => self::getCsrfTokenValue(self::getCsrfTokenId($id))]);
            }),
            new \Twig_SimpleFunction('formOpen', function($id, $action, $method = 'POST', $params = []) {
                $paramsHtml = '';
                foreach ($params as $key => $value) {
                    $paramsHtml .= $key . '="' . (string) $value . '" ';
                }
                $html = "";
                $html .= "<form id=\"" . $id . "\" action=\"" . $action . "\" method=\"" . $method . "\" " . $paramsHtml . ">\n";
                $html .= "<input type=\"hidden\" name=\"" .  self::getCsrfTokenId($id) . "\" value=\"" . self::getCsrfTokenValue($id) . "\">\n";
                return $html;
            }),
            new \Twig_SimpleFunction('formClose', function() {
                return "</form>";
            })

        );
    }


    /**
     * @return array|\Twig_NodeVisitorInterface[]
     */
    public function getNodeVisitors()
    {
        return parent::getNodeVisitors();
    }


    /**
     * @return array|\Twig_TokenParserInterface[]
     */
    public function getTokenParsers()
    {
        return parent::getTokenParsers();
    }


    /**
     * @return array
     */
    public function getOperators()
    {
        return parent::getOperators();
    }


    /**
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     * @return string
     */
    public static function priceFilter($number, $decimals = 0, $decPoint = '.', $thousandsSep = ',')
    {
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);
        $price = '£'.$price;

        return $price;
    }

    /**
     * @param $id
     * @return string
     */
    public static function getCsrfTokenId($id)
    {
        return WsApp::getInstance()->security()->getCsrfTokenId($id);
    }


    /**
     * @param $id
     * @return string
     */
    public static function getCsrfTokenValue($id)
    {
        return WsApp::getInstance()->security()->getCsrfToken(self::getCsrfTokenId($id));
    }
}

