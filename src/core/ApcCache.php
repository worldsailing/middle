<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use worldsailing\Helper\WsHelper;

/**
 * Class ApcCache
 * @package Core
 */
class ApcCache extends AbstractCache implements CacheInterface
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var string
     */
    public $namespace = '';

    /**
     * @var int
     */
    public $defaultExpiry = 0;

    /**
     * ApcCache constructor.
     * @param array $options
     */
    public function __construct( $options = array() )
    {
        function __construct()
        {
            $options = WsApp::getInstance()->config()->get('cache', 'ApcCache.options');

            $this->namespace = (isset($options['namespace']) && strlen($options['namespace']) > 0) ? $options['namespace'] : $this->namespace;

            if ( isset($options['defaultExpiry']) ) {
                $this->defaultExpiry = ((int)$options['defaultExpiry'] > 0 ) ? (int) $options['defaultExpiry'] : 0;
            }
        }
    }

    /**
     * @return ApcCache
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param $key
     * @param int $ttl
     * @param null $default
     * @return mixed
     */
    public function get($key, $ttl = 0, $default = null)
    {
        try {
            if ($result = apc_fetch( $this->namespace . ':' . $key )) {
                return $result;
            } else {
                return $default;
            }
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Apc error at reading cache [ApcCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            return $default;
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     */
    public function set($key, $value, $ttl = 0)
    {
        if( 0 === $ttl && $this->defaultExpiry ) {
            $ttl = $this->defaultExpiry;
        }
        try {
            apc_store( $this->namespace . ':' . $key , $value , $ttl );
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Apc error at writing cache [ApcCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     * @param string $key
     */
    public function remove($key)
    {
        try {
            apc_delete( $this->namespace . ':' . $key );
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Apc error at deleting cache [ApcCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     *
     */
    public function clear()
    {
        try {
            apc_clear_cache();
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Apc error at clearing cache [ApcCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
        }
        return;
    }

    /**
     * @param string $key
     * @return bool|\string[]
     */
    public function exists($key)
    {
        try {
            return apc_exists($key);
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->error('Apc error at reading cache [ApcCache]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            return false;
        }
    }
}

