<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

use WsApp;
use worldsailing\Helper\WsHelper;

/**
 * Class RequestValidation
 * @package Core
 */
class RequestValidation
{
    /**
     * @var WsApp
     */
    private $app;

    /**
     * @var
     */
    private $id;

    /**
     * @var string
     */
    private $message = '';

    /**
     * RequestValidation constructor.
     * @param WsApp $app
     * @param $id
     */
    public function __construct(WsApp $app, $id)
    {
        $this->app = $app;

        $this->id = $id;
    }

    /**
     * @param array $requestConfig
     * @return bool
     */
    public function validate($requestConfig)
    {
        if (! $this->app->config()->keyExists('request', 'methods') ) {
            $configKey = 'request' . WsApp::getInstance()->getDelimiter() . $this->id;
            $this->app->config()->load($configKey, 'request');
        }

        // Validate csrf if necessary
        if (isset($requestConfig['requiredCsrfToken']) && (strlen($requestConfig['requiredCsrfToken']) > 0)) {
            // Validate csrf token
            $tokenName = $this->app->security()->getCsrfTokenId($requestConfig['requiredCsrfToken']);
            if ($this->app->input()->keyExists($tokenName)) {
                $tokenValue = $this->app->input()->get($tokenName, '', true);
                if (! $this->app->security()->checkCsrfToken($tokenName, $tokenValue)) {
                    $this->message = 'Invalid CSRF token';
                }
            } else {
                $this->message = 'CSRF Token does not exist';
            }
        }
        return ($this->message === '') ? true : false;
    }

    /**
     * @return string
     */
    public function validationError()
    {
        return $this->message;
    }
}
