<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

/**
 * Class Csrf
 * @package Core
 */
class Csrf
{

    /**
     *
     */
    const EXPIRY = 1200; //20 mins total lifetime

    /**
     *
     */
    const REFRESH_TIME = 600;

    /**
     * @var
     */
    private $name;

    /**
     * @var array
     */
    private $tokens = [];

    /**
     * Csrf constructor.
     * @param $name
     * @param array $tokens
     */
    public function __construct($name, $tokens = [])
    {
        $this->name = $name;
        foreach ($tokens as $token) {
            $this->tokens[] = $token;
        }
        $this->removeAllExpired();
    }

    /**
     * @return string
     */
    public function createHash()
    {
        mt_srand();
        $hash = md5(time() + mt_rand(0, 1999999999));
        return $hash;
    }

    /**
     * @return \stdClass
     */
    private function refresh()
    {
        $token = new \stdClass();
        $token->value = $this->createHash();
        $token->created_at = time();
        $this->tokens[] = $token;
        return $token;
    }


    /**
     * @param \stdClass $token
     * @return bool
     */
    private function isExpired(\stdClass $token)
    {
        return ((time() - $token->created_at) > self::EXPIRY) ? true : false;
    }


    /**
     * @return $this
     */
    private function removeAllExpired()
    {
        foreach ($this->tokens as $key => $token) {
            if ($this->isExpired($token)) {
                unset($this->tokens[$key]);
            }
        }
        $this->sort();
        return $this;
    }


    /**
     * @return mixed
     */
    public function getLatestValueAndRefreshIfNecessary()
    {
        $this->removeAllExpired();
        if (count($this->tokens) > 0) {
            $latest = $this->tokens[count($this->tokens) - 1];
            if ((time() - $latest->created_at) > self::REFRESH_TIME) {
                $result = $this->refresh();
            } else {
                $result = $latest;
            }
        } else {
            $result = $this->refresh();
        }
        return $result->value;
    }


    /**
     * @param $value
     * @return bool
     */
    public function isValid($value)
    {
        $this->removeAllExpired();
        foreach ($this->tokens as $token) {
            if ($token->value == $value) {
                return true;
                break;
            }
        }
        return false;
    }


    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode([
            'name' => $this->name,
            'tokens' =>$this->tokens
        ]);
    }


    /**
     * @return $this
     */
    public function sort()
    {
        if (count($this->tokens) > 0) {
            $sorted = array();
            foreach ($this->tokens as $key => $token) {
                $sorted[$key] = $token->created_at;
            }
            array_multisort($sorted, SORT_ASC, $this->tokens);
        }
        return $this;
    }


}
