<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Core;

/**
 * Interface CacheInterface
 * @package Core
 */
interface CacheInterface
{
    /**
     * @param $key
     * @param int $ttl
     * @param null $default
     * @return mixed
     */
    public function get($key, $ttl = 0, $default = null);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $ttl
     * @return void
     */
    public function set($key, $value, $ttl = 0);

    /**
     * @param string $key
     * @return void
     */
    public function remove($key);

    /**
     * @return bool
     */
    public function clear();

    /**
     * @param $key
     * @return bool
     */
    public function exists($key);
}

