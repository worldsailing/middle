<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

use Controller\LayoutController;
use Controller\RequestController;
use Core\WsException;
use Service\ConfigService;
use Service\LogService;
use Service\InputService;
use Service\CacheService;
use Service\SecurityService;

/**
 * Class WsApp
 */
class WsApp {

    /**
     * @var
     */
    protected static $instance;

    /**
     * @var bool
     */
    private $visual = false;

    /**
     * @var string[]
     */
    private $validDelimiters = ['.', '/'];

    /**
     * @var string
     */
    private $pathDelimiter = '.';

    /**
     * WsApp constructor.
     */
    public function __construct()
    {
        define( 'WS_PATH_SRC', __DIR__);
        define( 'WS_PATH_ROOT', realpath(WS_PATH_SRC . '/..') );
        define( 'WS_ENVIRONMENT',  getenv('SYMFONY_ENV') ?: 'prod' ); //dev, test, prod

        require_once WS_PATH_ROOT . '/vendor/autoload.php';

        $this->init();
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }


    /**
     *
     */
    private function setVisual()
    {
        $this->visual = true;
    }


    /**
     * @return bool
     */
    public function isVisual()
    {
        return $this->visual;
    }


    /**
     *
     */
    private function init()
    {

    }

    /**
     * @param $path
     * @return string
     */
    public function setDelimiter($path)
    {
        foreach ($this->validDelimiters as $delimiter) {
            if (strpos($path, $delimiter) !== false) {
                $this->pathDelimiter = $delimiter;
                break;
            }
        }
        return $this->pathDelimiter;
    }


    /**
     * @param null $path
     * @return string
     */
    public function getDelimiter($path = null)
    {
        if ($path && strlen($path) > 0) {
            foreach ($this->validDelimiters as $delimiter) {
                if (strpos($path, $delimiter) !== false) {
                    return $delimiter;
                    break;
                }
            }
        }
        return $this->pathDelimiter;
    }


    /**
     * @param $dir
     * @return string
     */
    public function getDirPath($dir)
    {
        return str_replace('%WS_PATH_ROOT%', WS_PATH_ROOT, str_replace('%WS_PATH_SRC%' , WS_PATH_SRC, $dir));
    }


    /**
     * @return string
     */
    public function getRootUrl()
    {
        return $this->config()->get('common', 'urlParameters.protocol') . $this->config()->get('common', 'urlParameters.rootUrl');
    }


    /**
     * @param string $url
     * @return string
     */
    public function getAbsoluteUrl($url = '') {
        return $this->getRootUrl() . $url;
    }


    /**
     * @param $url
     * @return mixed
     */
    public function getUrl($url)
    {
        return $this->getDirPath(str_replace('%ROOT_URL%', $this->getRootUrl(), $url));
    }


    /**
     * @param $id
     * @param array $params
     */
    public function layout($id, $params = [])
    {
        try {
            $this->setVisual();
            $this->setDelimiter($id);
            $controller = new LayoutController($this);
            $controller->action($id, $params);
        } catch (WsException $e) {
            $this->log()->error($e->getMessage(), func_get_args());
            $e->show();
        } catch (\Exception $e) {
            $this->log()->error($e->getMessage(), func_get_args());
            $e = new WsException($e->getMessage(), $e->getCode());
            $e->show();
        }
    }


    /**
     * @param $id
     * @param array $params
     */
    public function request($id, $params = [])
    {
        try {
            $this->setDelimiter($id);
            $controller = new RequestController($this);
            $controller->action($id, $params);
        } catch (WsException $e) {
            $this->log()->error($e->getMessage(), func_get_args());
            $e->show();
        } catch (\Exception $e) {
            $this->log()->error($e->getMessage(), func_get_args());
            $e = new WsException($e->getMessage(), $e->getCode());
            $e->show();
        }
    }

    /**
     * @return ConfigService
     */
    public function config()
    {
        return ConfigService::getInstance();
    }


    /**
     * @return LogService
     */
    public function log()
    {
        return LogService::getInstance();
    }

    /**
     * @return InputService
     */
    public function input()
    {
        return InputService::getInstance();
    }

    /**
     * @return CacheService
     */
    public function cache()
    {
        return CacheService::getInstance();
    }

    /**
     * @return SecurityService
     */
    public function security()
    {
        return SecurityService::getInstance();
    }
}


