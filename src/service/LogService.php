<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Service;

use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

/**
 * Class LogService
 * @package Service
 */
class LogService {

    /**
     * @var
     */
    protected static $instance;

    /**
     * @var Logger
     */
    private $monolog;

    /**
     * @var bool
     */
    private $ready = false;

    /**
     * LogService constructor.
     */
    public function __construct()
    {
        $this->monolog = new Logger('default');
    }

    /**
     * @return LogService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @return $this
     */
    private function setReady()
    {
        $this->ready = true;
        return $this;
    }

    /**
     * @return bool
     */
    public function isReady()
    {
        return $this->ready;
    }

    /**
     * @param $path
     * @param int $maxFiles
     * @param int $level
     */
    public function loadHandler($path, $maxFiles = 10, $level = Logger::WARNING)
    {
        $this->monolog->pushHandler(new RotatingFileHandler($path, $maxFiles, $level));
        $this->setReady();
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->monolog;
    }

    /**
     * @param $method
     * @param null $arguments
     * @return bool|mixed
     */
    public function __call($method, $arguments = null){
        if ($this->ready) {
            if (method_exists($this->monolog, $method)) {
                return call_user_func_array(array($this->monolog, $method), $arguments);
            } else {
                return false;
            }
        }
    }



}
