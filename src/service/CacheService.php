<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Service;

use worldsailing\Helper\WsHelper;
use WsApp;
use Core\SessionCache;
use Core\WsException;
use Core\CacheInterface;
use Core\FileSystemCache;
use Core\ApcCache;
use Core\MemcacheCache;
use Core\MemcachedCache;

class CacheService
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var CacheInterface[]
     */
    private $storeContainer = [];

    /**
     *
     */
    const SESSION_CACHE = 'SessionCache';

    /**
     *
     */
    const MEMCACHE_CACHE = 'MemcacheCache';

    /**
     *
     */
    const MEMCACHED_CACHE = 'MemcachedCache';

    /**
     *
     */
    const FILESYSTEM_CACHE = 'FileSystemCache';

    /**
     *
     */
    const APC_CACHE = 'ApcCache';

    /**
     *
     */
    const REDIS_CACHE = 'RedisCache';

    /**
     * CacheService constructor.
     */
    public function __construct()
    {
        WsApp::getInstance()->config()->load('system.cache', 'cache');
    }

    /**
     * @return CacheService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param $name
     * @return bool
     */
    public static function isValidDriver($name)
    {
        $ref = new \ReflectionClass(get_called_class());
        $drivers =  $ref->getConstants();
        foreach ($drivers as $driver) {
            if ($name == $driver) {
                return true;
                break;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public static function getCacheId()
    {
        return 'chid' . ((String) preg_replace("/[^a-zA-Z0-9]+/", "", urlencode($_SERVER['REQUEST_URI'])));
    }

    /**
     * @param $className
     * @param $alias
     * @return CacheInterface
     * @throws WsException
     */
    public function registerCache( $className, $alias)
    {
        if ((! is_string($alias)) || (strlen($alias) == 0 )) {
            throw new WsException('Invalid cache alias [' . $alias . ']');
        }
        if (isset($this->storeContainer[$alias])) {
            throw new WsException('Alias already exists in cache container [' . $alias . ']');
        }
        switch ($className) {
            case self::SESSION_CACHE:
                $this->storeContainer[$alias] = new SessionCache();
                break;
            case self::FILESYSTEM_CACHE:
                $this->storeContainer[$alias] = new FileSystemCache();
                break;
            case self::APC_CACHE:
                if (! function_exists('apc_fetch')) {
                    throw new WsException('function apc_fetch() does not exist [CacheService]');
                }
                $this->storeContainer[$alias] = new ApcCache();
                break;
            case self::MEMCACHE_CACHE:
                if(! class_exists('\Memcache')){
                    throw new WsException('Class \Memcache does not exist [CacheService]');
                }
                $this->storeContainer[$alias] = new MemcacheCache();
                break;
            case self::MEMCACHED_CACHE:
                if(! class_exists('\Memcached')){
                    throw new WsException('Class \Memcached does not exist [CacheService]');
                }
                $this->storeContainer[$alias] = new MemcachedCache();
                break;
            default:
                throw new WsException('Invalid cache driver [' . $className . ']');
                break;
        }
        return $this->storeContainer[$alias];
    }

    /**
     * @param $alias
     * @return CacheInterface
     * @throws WsException
     */
    public function storage($alias)
    {
        if (isset($this->storeContainer[$alias])) {
            return $this->storeContainer[$alias];
        } else {
            throw new WsException('Unregistered cache driver [' . $alias . ']');
        }
    }


    /**
     * @param $alias
     * @return bool
     */
    public function isRegistered($alias)
    {
        return isset($this->storeContainer[$alias]);
    }


    /**
     * @param $alias
     */
    public function unRegisterCache($alias)
    {
        if (isset($this->storeContainer[$alias])) {
            $this->storeContainer[$alias]->clear();
            unset($this->storeContainer[$alias]);
            return;
        } else {
            WsApp::getInstance()->log()->warning('Unregistered cache driver detected', ['alias' => $alias]);
        }
    }

    /**
     *
     */
    public function clearAll()
    {
        foreach ($this->storeContainer as $cache) {
            $cache->clear();
        }
        return;
    }

    /**
     *
     */
    public function unRegisterAll()
    {
        $this->clearAll();
        $this->storeContainer = [];
    }


}
