<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Service;

use WsApp;
use Core\Csrf;

/**
 * Class SecurityService
 * @package Service
 */
class SecurityService
{

    /**
     * @var SecurityService
     */
    protected static $instance;

    /**
     * @var array
     */
    private $neverAllowedStr = array(
        'document.cookie' => '[removed]',
        'document.write' => '[removed]',
        '.parentNode' => '[removed]',
        '.innerHTML' => '[removed]',
        'window.location' => '[removed]',
        '-moz-binding' => '[removed]',
        '<!--' => '&lt;!--',
        '-->' => '--&gt;',
        '<![CDATA[' => '&lt;![CDATA[',
        '<comment>' => '&lt;comment&gt;'
    );

    /**
     * @var string[]
     */
    private $neverAllowedRegex = array(
        'javascript\s*:',
        'expression\s*(\(|&\#40;)', // CSS and IE
        'vbscript\s*:', // IE, surprise!
        'Redirect\s+302',
        "([\"'])?data\s*:[^\\1]*?base64[^\\1]*?,[^\\1]*?\\1?"
    );

    /**
     * @var string
     */
    private $xssHash;


    /**
     * SecurityService constructor.
     */
    public function __construct()
    {
        WsApp::getInstance()->cache()->registerCache('SessionCache', 'csrf');
    }


    /**
     * @return SecurityService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param $id
     * @return string
     */
    public function getCsrfTokenId($id)
    {
        return 'csrf-' . $id;
    }

    /**
     * @param $csrfId
     * @return string
     */
    private function registerCsrfToken($csrfId)
    {
        $csrf = new Csrf($csrfId, []);
        $token = $csrf->getLatestValueAndRefreshIfNecessary();
        WsApp::getInstance()->cache()->storage('csrf')->set($csrfId, $csrf->toJson());
        return $token;
    }

    /**
     * @param $csrfId
     * @return string
     */
    public function getCsrfToken($csrfId)
    {
        if ($meta = WsApp::getInstance()->cache()->storage('csrf')->get($csrfId)) {
            if ($meta !== null) {
                $meta = json_decode($meta);
                $csrf = new Csrf($csrfId, (array) $meta->tokens);
                $token =  $csrf->getLatestValueAndRefreshIfNecessary();
                WsApp::getInstance()->cache()->storage('csrf')->set($csrfId, $csrf->toJson());
            } else {
                $token =  $this->registerCsrfToken($csrfId);
            }
        } else {
            $token = $this->registerCsrfToken($csrfId);
        }
        return $token;
    }


    /**
     * @param $csrfId
     * @param $value
     * @return bool
     */
    public function checkCsrfToken($csrfId, $value)
    {
        if ($value && strlen($value) > 0) {
            if ($meta = WsApp::getInstance()->cache()->storage('csrf')->get($csrfId)) {
                $meta = json_decode($meta);
                $csrf = new Csrf($csrfId, (array) $meta->tokens);
                return $csrf->isValid($value);
            }
        }
        return false;
    }


    /**
     * @return string
     */
    public function createTimeHash()
    {
        return (string) preg_replace("/[^a-zA-Z0-9]+/", "", Date("Y-m-d H:i:s"));
    }

    /**
     * @return string
     */
    public function createHash()
    {
        mt_srand();
        $hash = md5(time() + mt_rand(0, 1999999999));
        return $hash;
    }

    /**
     * @param $str
     * @return array|string
     */
    public function escapeStr($str){

        if (is_array($str))
        {
            foreach ($str as $key => $val)
            {
                $str[$key] = $this->escapeStr($val);
            }

            return $str;
        }

        $str = addslashes($str);

        return $str;
    }

    /**
     * @param $str
     * @return string
     */
    public function quotedStr($str){
        return "'" . $this->escapeStr($str) . "'";
    }


    /**
     * Attribute Conversion
     *
     * Used as a callback for XSS Clean
     *
     * @param    array
     * @return    string
     */
    public function convertAttribute($match)
    {
        return str_replace(array('>', '<', '\\'), array('&gt;', '&lt;', '\\\\'), $match[0]);
    }

    /**
     * HTML Entities Decode
     *
     * This function is a replacement for html_entity_decode()
     *
     * The reason we are not using html_entity_decode() by itself is because
     * while it is not technically correct to leave out the semicolon
     * at the end of an entity most browsers will still interpret the entity
     * correctly.  html_entity_decode() does not convert entities without
     * semicolons, so we are left with our own little solution here. Bummer.
     *
     * @param    string
     * @param    string
     * @return    string
     */
    public function entityDecode($str, $charset = 'UTF-8')
    {
        if (is_array($str)) {
            $a = '';
            foreach ($str as $value) {
                $a .= $value;
            }
            $str = $a;
        }
        if (stristr($str, '&') === true) {
            return $str;
        }

        $str = html_entity_decode($str, ENT_COMPAT, $charset);
        $str = preg_replace_callback('~&#x(0*[0-9a-f]{2,5})~i', create_function('$matches', 'return chr(hexdec($matches[2]));'), $str);
        return preg_replace_callback('~&#([0-9]{2,4})~', create_function('$matches', 'return chr($matches[1]);'), $str);
    }

    /**
     * Compact Exploded Words
     *
     * Callback function for xss_clean() to remove whitespace from
     * things like j a v a s c r i p t
     *
     * @param $matches
     * @return string
     */
    public function compactExplodedWords($matches)
    {
        return preg_replace('/\s+/s', '', $matches[1]) . $matches[2];
    }

    /**
     * JS Link Removal
     *
     * Callback function for xss_clean() to sanitize links
     * This limits the PCRE backtracks, making it more performance friendly
     * and prevents PREG_BACKTRACK_LIMIT_ERROR from being triggered in
     * PHP 5.2+ on link-heavy strings
     *
     * @param    array
     * @return    string
     */
    public function jsLinkRemoval($match)
    {
        return str_replace(
            $match[1],
            preg_replace(
                '#href=.*?(alert\(|alert&\#40;|javascript\:|livescript\:|mocha\:|charset\=|window\.|document\.|\.cookie|<script|<xss|data\s*:)#si',
                '',
                $this->filterAttributes(str_replace(array('<', '>'), '', $match[1]))
            ),
            $match[0]
        );
    }

    /**
     * Filter Attributes
     *
     * Filters tag attributes for consistency and safety
     *
     * @param    string
     * @return    string
     */
    public function filterAttributes($str)
    {
        $out = '';

        if (preg_match_all('#\s*[a-z\-]+\s*=\s*(\042|\047)([^\\1]*?)\\1#is', $str, $matches)) {
            foreach ($matches[0] as $match) {
                $out .= preg_replace("#/\*.*?\*/#s", '', $match);
            }
        }

        return $out;
    }

    /**
     * JS Image Removal
     *
     * Callback function for xss_clean() to sanitize image tags
     * This limits the PCRE backtracks, making it more performance friendly
     * and prevents PREG_BACKTRACK_LIMIT_ERROR from being triggered in
     * PHP 5.2+ on image tag heavy strings
     *
     * @param    array
     * @return    string
     */
    public function jsImgRemoval($match)
    {
        return str_replace(
            $match[1],
            preg_replace(
                '#src=.*?(alert\(|alert&\#40;|javascript\:|livescript\:|mocha\:|charset\=|window\.|document\.|\.cookie|<script|<xss|base64\s*,)#si',
                '',
                $this->filterAttributes(str_replace(array('<', '>'), '', $match[1]))
            ),
            $match[0]
        );
    }

    /**
     * Sanitize Naughty HTML
     *
     * Callback function for xss_clean() to remove naughty HTML elements
     *
     * @param    array
     * @return    string
     */
    public function sanitizeNaughtyHtml($matches)
    {
        // encode opening brace
        $str = '&lt;' . $matches[1] . $matches[2] . $matches[3];

        // encode captured opening or closing brace to prevent recursive vectors
        $str .= str_replace(array('>', '<'), array('&gt;', '&lt;'),
            $matches[4]);

        return $str;
    }


    /**
     * Returns HTML escaped variable
     *
     * @access    public
     * @param    mixed
     * @return    mixed
     */
    public function htmlEscape($var, $charset = 'UTF-8')
    {
        if (is_array($var)) {
            return array_map(array($this, 'htmlEscape'), $var);
        } else {
            return htmlspecialchars($var, ENT_QUOTES, $charset);
        }
    }

    /**
     * Clean file content
     *
     * An extend function of xssClean()
     *
     * @param    string
     * @return    string
     */
    public function xssCleanImage($filename)
    {
        if (is_file($filename)) {
            $content = file_get_contents($filename);
            return $this->xssClean($content, true);
        } else {
            return false;
        }
    }


    /**
     * Remove Invisible Characters
     *
     * This prevents sandwiching null characters
     * between ascii characters, like Java\0script.
     *
     * @access    public
     * @param    string
     * @return    string
     */
    public function removeInvisibleCharacters($str, $url_encoded = true)
    {
        $non_displayables = array();

        // every control character except newline (dec 10)
        // carriage return (dec 13), and horizontal tab (dec 09)

        if ($url_encoded) {
            $non_displayables[] = '/%0[0-8bcef]/';    // url encoded 00-08, 11, 12, 14, 15
            $non_displayables[] = '/%1[0-9a-f]/';    // url encoded 16-31
        }

        $non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';    // 00-08, 11, 12, 14-31, 127

        do {
            $str = preg_replace($non_displayables, '', $str, -1, $count);
        } while ($count);

        return $str;
    }


    /**
     * Validate URL entities
     *
     * Called by xss_clean()
     *
     * @param    string
     * @return    string
     */
    public function validateEntities($str, $xssHash)
    {
        /*
         * Protect GET variables in URLs
         */

        // 901119URL5918AMP18930PROTECT8198

        $str = preg_replace('|\&([a-z\_0-9\-]+)\=([a-z\_0-9\-]+)|i', $xssHash . "\\1=\\2", $str);

        /*
         * Validate standard character entities
         *
         * Add a semicolon if missing.  We do this to enable
         * the conversion of entities to ASCII later.
         *
         */
        $str = preg_replace('#(&\#?[0-9a-z]{2,})([\x00-\x20])*;?#i', "\\1;\\2", $str);

        /*
         * Validate UTF16 two byte encoding (x00)
         *
         * Just as above, adds a semicolon if missing.
         *
         */
        $str = preg_replace('#(&\#x?)([0-9A-F]+);?#i', "\\1\\2;", $str);

        /*
         * Un-Protect GET variables in URLs
         */
        $str = str_replace($xssHash, '&', $str);

        return $str;
    }

    /**
     * Do Never Allowed - Codeigniter
     *
     * A utility function for xssClean()
     *
     * @param    string
     * @return    string
     */
    public function sanitizeNeverAllowed($str)
    {
        $str = str_replace(array_keys($this->neverAllowedStr), $this->neverAllowedStr, $str);

        foreach ($this->neverAllowedRegex as $regex) {
            $str = preg_replace('#' . $regex . '#is', '[removed]', $str);
        }

        return $str;
    }

    /**
     * Remove Evil HTML Attributes (like evenhandlers and style)
     *
     * It removes the evil attribute and either:
     * 	- Everything up until a space
     *		For example, everything between the pipes:
     *		<a |style=document.write('hello');alert('world');| class=link>
     * 	- Everything inside the quotes
     *		For example, everything between the pipes:
     *		<a |style="document.write('hello'); alert('world');"| class="link">
     *
     * @param string $str The string to check
     * @param boolean $is_image true if this is an image
     * @return string The string with the evil attributes removed
     */
    public function removeEvilAttributes($str, $is_image = false)
    {
        // All javascript event handlers (e.g. onload, onclick, onmouseover), style, and xmlns
        $evil_attributes = array('on\w*', 'style', 'xmlns', 'formaction');

        if ($is_image === true) {
            /*
             * Adobe Photoshop puts XML metadata into JFIF images,
             * including namespacing, so we have to allow this for images.
             */
            unset($evil_attributes[array_search('xmlns', $evil_attributes)]);
        }

        do {
            $count = 0;
            $attribs = array();

            // find occurrences of illegal attribute strings without quotes
            preg_match_all('/(' . implode('|', $evil_attributes) . ')\s*=\s*([^\s>]*)/is', $str, $matches, PREG_SET_ORDER);

            foreach ($matches as $attr) {

                $attribs[] = preg_quote($attr[0], '/');
            }

            // find occurrences of illegal attribute strings with quotes (042 and 047 are octal quotes)
            preg_match_all("/(" . implode('|', $evil_attributes) . ")\s*=\s*(\042|\047)([^\\2]*?)(\\2)/is", $str, $matches, PREG_SET_ORDER);

            foreach ($matches as $attr) {
                $attribs[] = preg_quote($attr[0], '/');
            }

            // replace illegal attribute strings that are inside an html tag
            if (count($attribs) > 0) {
                $str = preg_replace("/<(\/?[^><]+?)([^A-Za-z<>\-])(.*?)(" . implode('|', $attribs) . ")(.*?)([\s><])([><]*)/i", '<$1 $3$5$6$7', $str, -1, $count);
            }

        } while ($count);

        return $str;
    }


    /**
     * Random Hash for protecting URLs
     *
     * @return    string
     */
    private function getXssHash()
    {
        if ($this->xssHash == '') {
            $this->xssHash = $this->createHash();
        }

        return $this->xssHash;
    }


    /**
     * XSS Clean
     *
     * Sanitizes data so that Cross Site Scripting Hacks can be
     * prevented.  This function does a fair amount of work but
     * it is extremely thorough, designed to prevent even the
     * most obscure XSS attempts.  Nothing is ever 100% foolproof,
     * of course, but I haven't been able to get anything passed
     * the filter.
     *
     * Note: This function should only be used to deal with data
     * upon submission.  It's not something that should
     * be used for general runtime processing.
     *
     * This function was based in part on some code and ideas I
     * got from Bitflux: http://channel.bitflux.ch/wiki/XSS_Prevention
     *
     * To help develop this script I used this great list of
     * vulnerabilities along with a few other hacks I've
     * harvested from examining vulnerabilities in other programs:
     * http://ha.ckers.org/xss.html
     *
     * @param    mixed    string or array
     * @param    bool
     * @return    string
     */
    public function xssClean($str, $is_image = false)
    {
        $this->getXssHash();

        if (is_array($str)) {
            while (list($key) = each($str)) {
                $str[$key] = $this->xssClean($str[$key]);
            }

            return $str;
        }

        $str = $this->removeInvisibleCharacters($str);

        $str = $this->validateEntities($str, $this->xssHash);

        $str = rawurldecode($str);

        $str = preg_replace_callback("/[a-z]+=([\'\"]).*?\\1/si", array($this, 'convertAttribute'), $str);

        $str = preg_replace_callback("/<\w+.*?(?=>|<|$)/si", array($this, 'entityDecode'), $str);

        $str = $this->removeInvisibleCharacters($str);

        if (strpos($str, "\t") !== true) {
            $str = str_replace("\t", ' ', $str);
        }

        /*
         * Capture converted string for later comparison
         */
        $converted_string = $str;

        $str = $this->sanitizeNeverAllowed($str);

        /*
         * Makes PHP tags safe
        */
        $str = str_replace(array('<?', '?' . '>'), array('&lt;?', '?&gt;'), $str);


        /*
         * Compact any exploded words
         *
         * This corrects words like:  j a v a s c r i p t
         * These words are compacted back to their correct state.
         */
        $words = array(
            'javascript', 'expression', 'vbscript', 'script', 'base64',
            'applet', 'alert', 'document', 'write', 'cookie', 'window'
        );

        foreach ($words as $word) {
            $temp = '';

            for ($i = 0, $wordlen = strlen($word); $i < $wordlen; $i++) {
                $temp .= substr($word, $i, 1) . "\s*";
            }

            $str = preg_replace_callback('#(' . substr($temp, 0, -3) . ')(\W)#is', array($this, 'compactExplodedWords'), $str);
        }

        /*
         * Remove disallowed Javascript in links or img tags
         */
        do {
            $original = $str;

            if (preg_match("/<a/i", $str)) {
                $str = preg_replace_callback("#<a\s+([^>]*?)(>|$)#si", array($this, 'jsLinkRemoval'), $str);
            }

            if (preg_match("/<img/i", $str)) {
                $str = preg_replace_callback("#<img\s+([^>]*?)(\s?/?>|$)#si", array($this, 'jsImgRemoval'), $str);
            }

            if (preg_match("/script/i", $str) || preg_match("/xss/i", $str)) {
                $str = preg_replace("#<(/*)(script|xss)(.*?)\>#si", '[removed]', $str);
            }
        } while ($original != $str);

        unset($original);

        $str = $this->removeEvilAttributes($str, $is_image);

        /*
         * Sanitize naughty HTML elements
         *
         * If a tag containing any of the words in the list
         * below is found, the tag gets converted to entities.
         *
         * So this: <blink>
         * Becomes: &lt;blink&gt;
         */
        $naughty = 'alert|applet|audio|basefont|base|behavior|bgsound|blink|body|embed|expression|form|frameset|frame|head|html|ilayer|iframe|input|isindex|layer|link|meta|object|plaintext|style|script|textarea|title|video|xml|xss';
        $str = preg_replace_callback('#<(/*\s*)(' . $naughty . ')([^><]*)([><]*)#is', array($this, 'sanitizeNaughtyHtml'), $str);

        /*
         * Sanitize naughty scripting elements
         *
         * Similar to above, only instead of looking for
         * tags it looks for PHP and JavaScript commands
         * that are disallowed.  Rather than removing the
         * code, it simply converts the parenthesis to entities
         * rendering the code un-executable.
         *
         * For example:	eval('some code')
         * Becomes:		eval&#40;'some code'&#41;
         */
        $str = preg_replace('#(alert|cmd|passthru|eval|exec|expression|system|fopen|fsockopen|file|file_get_contents|readfile|unlink)(\s*)\((.*?)\)#si', "\\1\\2&#40;\\3&#41;", $str);

        $str = $this->sanitizeNeverAllowed($str);

        if ($is_image === true) {
            return ($str == $converted_string) ? true : true;
        }

        return $str;
    }

}
