<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Service;

use worldsailing\Helper\WsHelper;
use WsApp;

/**
 * Class InputService
 * @package Service
 */
class InputService
{

    /**
     * @var
     */
    protected static $instance;

    /**
     * @var
     */
    private $data = [];

    /**
     * @var bool
     */
    private $xssFiltered = false;

    /**
     * Load all inputs from global array depending on request method.
     *
     * InputService constructor.
     */
    public function __construct()
    {
        try {
            $this->load(WsApp::getInstance()->config()->get('common', 'security.globalXss') === true); //Apply global xss filtering - if enabled
        } catch(\Exception $e) {
            WsApp::getInstance()->log()->warning('XSS filtering has not been success [InputService]', WsHelper::getExceptionContext($e, __FILE__, __LINE__));
            $this->load(false);
        }
    }

    /**
     * @return InputService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }

    /**
     * @param $xssClean
     * @return bool
     */
    private function shouldApplyXssFilter($xssClean)
    {
        if ($this->xssFiltered === true) {
            return false;
        } else {
            return ($xssClean === true) ? true : false;
        }
    }

    /**
     * @return bool|string
     */
    private function getContent()
    {
        if (0 === strlen(trim($content = file_get_contents('php://input')))){
            $content = null;
        }

        return $content;
    }

    /**
     * @param bool $xssClean
     * @return $this
     */
    public function load($xssClean = false)
    {
        switch (WsHelper::getHttpMethod()) {
            case 'GET': $this->data = $_GET;
                break;
            case 'POST': $this->data = $_POST;
                break;
            case 'PUT': parse_str($this->getContent(), $this->data);
                break;
            case 'DELETE': parse_str($this->getContent(), $this->data);
                break;
        }
        if ($xssClean === true) {
            foreach ($this->data as $key => $value) {
                $this->data[$key] = WsApp::getInstance()->security()->xssClean($value);
            }
            $this->xssFiltered = true; //Trusted data
        } else {
            $this->xssFiltered = false; //Not trusted data
        }
        return $this;
    }

    /**
     * @return array
     */
    public function keys()
    {
        if ($this->data && is_array($this->data)) {
            return array_keys($this->data);
        } else {
            return [];
        }
    }


    /**
     * @return int
     */
    public function count()
    {
        if ($this->data && is_array($this->data)) {
            return count($this->data);
        } else {
            return 0;
        }
    }


    /**
     * @param $key
     * @return bool
     */
    public function keyExists($key)
    {
        if ($this->data && is_array($this->data)) {
            return isset($this->data[$key]);
        } else {
            return false;
        }
    }


    /**
     * @param $key
     * @return $this
     */
    public function delete($key)
    {
        if ($this->data && is_array($this->data)) {
            if (isset($this->data[$key])) {
                unset($this->data[$key]);
            }
        }
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @param bool $xssClean
     * @return InputService
     */
    public function add($key, $value, $xssClean = false)
    {
        if ($this->data && is_array($this->data)) {
            $this->data[$key] = ($xssClean === true) ? WsApp::getInstance()->security()->xssClean($value) : $value;
            if ($xssClean !== true) {
                $this->xssFiltered = false; //Not trusted any more.
            }
            return $this;
        } else {
            $this->data = [];
            return $this->add($key, $value, $xssClean);
        }
    }


    /**
     * @param $key
     * @param string $default
     * @param bool $xssClean
     * @return mixed
     */
    public function get($key, $default = '', $xssClean = false)
    {
        if ($this->data && is_array($this->data)) {
            if ($this->keyExists($key)) {
                return ($this->shouldApplyXssFilter($xssClean)) ? WsApp::getInstance()->security()->xssClean($this->data[$key]) : $this->data[$key];
            } else {
                return $default;
            }
        } else {
            return $default;
        }
    }


    /**
     * @param bool $xssClean
     * @return array
     */
    public function fetch($xssClean = false)
    {
        if ($this->shouldApplyXssFilter($xssClean)) {
            $data = [];
            foreach ($this->data as $key => $value) {
                $data[$key] = WsApp::getInstance()->security()->xssClean($value);
            }
            return $data;
        } else {
            return $this->data;
        }
    }

}
