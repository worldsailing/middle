<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Service;

use WsApp;
use Core\WsException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ConfigService
 * @package Service
 */
class ConfigService
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var array
     */
    private $configRepo = [];

    /**
     * @return ConfigService
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new static;
        }
        return self::$instance;
    }


    /**
     * @param $configPattern
     * @param $alias
     * @param bool $override
     * @return $this
     * @throws WsException
     */
    public function load($configPattern, $alias, $override = false)
    {
        if (strlen($alias) == 0) {
            $alias = $configPattern;
        }
        if ((! isset($this->configRepo[$alias])) || $override === true) {

            if (is_string($configPattern) && strlen($configPattern) > 0) {
                if ($delimiter = WsApp::getInstance()->getDelimiter($configPattern)) {
                    $a = explode($delimiter, $configPattern);

                    $config = ($delimiter === '/') ? $configPattern : str_replace($delimiter, '/', $configPattern) . '.yml';

                    if ($a[0] == 'system') {
                        if (is_file(WS_PATH_ROOT . '/config/' . str_replace('system/', 'system/' . WS_ENVIRONMENT . '/', $config))) {
                            $config = str_replace('system/', 'system/' . WS_ENVIRONMENT . '/', $config);
                        }
                    }

                    if (is_file(WS_PATH_ROOT . '/config/' . $config)) {

                        $this->configRepo[$alias] = Yaml::parse(file_get_contents(WS_PATH_ROOT . '/config/' . $config));

                    } else {
                        throw new WsException('Config file does not exist [' . $configPattern . ']');
                    }
                } else {
                    throw new WsException('Config file does not exist [' . $configPattern . ']');
                }
            } else {
                throw new WsException('Invalid config pattern');
            }
        }

        return $this;
    }


    /**
     * @param $configAlias
     * @param $item
     * @return bool
     */
    public function keyExists($configAlias, $item)
    {
        try {
            $this->get($configAlias, $item);
            return true;
        } catch (WsException $e) {
            return false;
        }
    }


    /**
     * @param $configAlias
     * @param $item
     * @return array|mixed
     * @throws WsException
     */
    public function get($configAlias, $item)
    {
        $delimiter = WsApp::getInstance()->getDelimiter($item);

        if (strpos($item, $delimiter) !== false) {
            $item = explode($delimiter, $item);
        }

        if (is_array($item)){
            $tmp = $this->getElement($configAlias);
            foreach ($item as $element) {
                $tmp = $this->getElement($element, $tmp);
            }
            return $tmp;
        } else {
            if (isset($this->configRepo[$configAlias][$item])) {
                return $this->configRepo[$configAlias][$item];
            } else {
                throw new WsException('Key [' . $item .'] does not exist in config [' . $configAlias .']');
            }
        }
    }


    /**
     * @param $item
     * @param null $array
     * @return mixed
     * @throws WsException
     */
    public function getElement($item, $array = null) {
        if ($array === null) {
            $array = $this->configRepo;
        }
        if (isset($array[$item])) {
            return $array[$item];
        } else {
            throw new WsException('Key [' . $item .'] does not exist in config');
        }
    }


}
