<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Process\example;

use Service\CacheService;
use WsApp;
use Core\RestProcessInterface;
use Core\RestProtocolResult;

use worldsailing\Api\core\WsApiException;
use worldsailing\Api\core\Token;
use worldsailing\Api\example\Example;

/**
 * Class ExampleEntity
 * @package Process\example
 */
class ExampleEntity implements RestProcessInterface
{

    public static $tokenCacheId = 'token.example.entity';

    public static $dataCacheId = 'data.example.entity';

    /**
     * @param WsApp $app
     * @param array $parameters
     * @returns RestProtocolResult
     */
    public function run(WsApp $app, $parameters = [])
    {
        //load authentication config
        $app->config()->load('system.authentication', 'auth');
        $credentials = $app->config()->get('auth', 'credentials');
        $credentials['environment'] = WS_ENVIRONMENT;

        //Refresh cache if necessary
        if (isset($parameters['nocache']) && $parameters['nocache'] == '1') {
            $app->cache()->storage('data_cache')->remove(self::$dataCacheId);
        }

        //Find cached result
        if ($app->config()->get('common', 'dataCacheEnabled') === true) {

            if ($app->cache()->storage('data_cache')->exists(self::$dataCacheId) ) {

                $dataCache = $app->cache()->storage('data_cache')->get(self::$dataCacheId);

                if (isset($dataCache[CacheService::getCacheId()])) {
                    return (new RestProtocolResult())->setData(json_encode($dataCache[CacheService::getCacheId()]))
                        ->cached(true);
                }
            }
        }

        Example::setLogger($app->log()->getLogger());

        Example::setCredentials($credentials);

        //Find cached Token
        if ($app->config()->get('common', 'tokenCacheEnabled') === true) {
            if ($app->cache()->storage('token_cache')->exists(self::$tokenCacheId) ) {
                $tokenValue = $app->cache()->storage('token_cache')->get(self::$tokenCacheId);
                Example::token(new Token($tokenValue, true));
            }
        }

        try {
            $entity = Example::getEntity($parameters);
            $result = (new RestProtocolResult())->setData($entity)
                ->code(Example::getLastResponse()->code())
                ->message(Example::getLastResponse()->message());

            //Cache Token
            if ($app->config()->get('common', 'tokenCacheEnabled') === true) {
                $token = Example::getToken();
                $app->cache()->storage('token_cache')->set(self::$tokenCacheId, $token->getValue());
            }

            //Cache result
            if ($app->config()->get('common', 'dataCacheEnabled') === true) {
                if ($app->cache()->storage('data_cache')->exists(self::$dataCacheId) ) {

                    $dataCache = $app->cache()->storage('data_cache')->get(self::$dataCacheId);
                    $dataCache[CacheService::getCacheId()] = json_decode($entity, true);
                } else {
                    $dataCache = [];
                    $dataCache[CacheService::getCacheId()] = json_decode($entity, true);
                }
                $app->cache()->storage('data_cache')->set(self::$dataCacheId, $dataCache);
            }

        } catch (WsApiException $e) {
            $result = (new RestProtocolResult())->success(false)
                ->code($e->getCode())
                ->message($e->getMessage());

            if (Example::getLastResponse() && Example::getLastResponse() instanceof RestProtocolResult) {
                $result->setData(Example::getLastResponse()->getBody());
            }
        } catch (\Exception $e) {
            $result = (new RestProtocolResult())->success(false)
                ->code($e->getCode())
                ->message($e->getMessage());
        }

        return $result;
    }
}

