<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Process\example;

use worldsailing\Api\core\WsApiException;
use WsApp;
use Core\RestProcessInterface;
use Core\RestProtocolResult;

use worldsailing\Api\example\Example;

/**
 * Class ExampleSave
 * @package Process\example
 */
class ExampleSave implements RestProcessInterface
{

    /**
     * @param WsApp $app
     * @param array $parameters
     * @returns RestProtocolResult
     */
    public function run(WsApp $app, $parameters = [])
    {
        //load authentication config
        $app->config()->load('system.authentication', 'auth');
        $credentials = $app->config()->get('auth', 'credentials');
        $credentials['environment'] = WS_ENVIRONMENT;

        Example::setLogger($app->log()->getLogger());

        Example::setCredentials($credentials);

        try {
            $entity = Example::save($parameters);
            $result = (new RestProtocolResult())->setData($entity)
                ->code(Example::getLastResponse()->code())
                ->message(Example::getLastResponse()->message());

            /**
             * Clear all affected caches after a successful modification
             */
            if ($app->config()->get('common', 'dataCacheEnabled') === true) {
                $app->cache()->storage('data_cache')->remove(ExampleList::$dataCacheId);
                $app->cache()->storage('data_cache')->remove(ExampleEntity::$dataCacheId);
            }
        } catch (WsApiException $e) {
            $result = (new RestProtocolResult())->success(false)
                ->code($e->getCode())
                ->message($e->getMessage());

            if (Example::getLastResponse() && Example::getLastResponse() instanceof RestProtocolResult) {
                $result->setData(Example::getLastResponse()->getBody());
            }
        } catch (\Exception $e) {
            $result = (new RestProtocolResult())->success(false)
                ->code($e->getCode())
                ->message($e->getMessage());
        }

        return $result;
    }
}

