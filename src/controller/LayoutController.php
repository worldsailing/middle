<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use WsApp;
use Core\Widget;

/**
 * Class LayoutController
 * @package Controller
 */
class LayoutController extends AbstractController
{

    /**
     * @var string
     */
    private $theme = 'default';

    /**
     * @var array
     */
    private $parameters = [];


    /**
     * LayoutController constructor.
     * @param WsApp $app
     */
    public function __construct(WsApp $app)
    {
        parent::__construct($app);

        if ($this->app->config()->keyExists('common', 'themeName')) {
            $this->theme = $this->app->config()->get('common', 'themeName');
        }
    }


    /**
     * @param $id
     * @param array $params
     */
    public function action($id, $params = [])
    {
        $this->app->log()->debug('Layout action',  ['theme' => $this->theme, 'id' => $id, 'params' => $params]);

        // Load configuration
        $configKey = 'layout' . $this->app->getDelimiter()  . $id;
        $this->app->config()->load($configKey, 'layout');

        // Collect url parameters
        $this->mergeParams([
            'webAppUrl' => $this->app->getUrl($this->app->config()->get('common', 'urlParameters.webAppRoot')),
            'bowerUrl' => $this->app->getUrl($this->app->config()->get('common', 'urlParameters.bowerRoot'))
        ]);

        // Collect predefined parameters
        $this->mergeParams($this->app->config()->get('layout', 'html.parameters'));

        // Collect request parameters - it overrides predefined parameters
        $this->mergeParams($this->app->input()->fetch());

        // Collect PHP parameters - it overrides any other predefined and collected parameters
        $this->mergeParams($params);

        // Log parameters
        $this->app->log()->debug('Collected parameters', $this->parameters);

        // Find template
        $template = $this->app->config()->get('layout', 'html.template');
        (new Widget($this->theme, $template, $this->parameters))->dump();
    }


    /**
     * @param array $params
     * @return array
     */
    private function mergeParams($params = [])
    {
        if ($params && is_array($params)) {
            foreach ($params as $key => $param) {
                $this->parameters[$key] = $param;
            }
        }
        return $this->parameters;
    }

}
