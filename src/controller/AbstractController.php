<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use worldsailing\Helper\WsHelper;
use WsApp;
use Monolog\Logger;

/**
 * Class AbstractController
 * @package Controller
 */
abstract class AbstractController
{

    /**
     * @var WsApp
     */
    protected $app;

    /**
     * @var
     */
    protected $data;

    /**
     * AbstractController constructor.
     * @param WsApp $app
     */
    public function __construct(WsApp $app)
    {
        $this->app = $app;

        $this->app->config()->load('system.common', 'common');
        $logFileName = (WsApp::getInstance()->isVisual()) ? 'layout.log' : 'request.log';
        $logFileNameAndPath = $this->app->config()->get('common', 'directories.pathLog') . '/' . $logFileName;
        $logLevel = $this->app->config()->get('common', 'monolog.logLevel');
        $maxFiles = $this->app->config()->get('common', 'monolog.maxFiles');
        $this->app->log()->loadHandler($this->app->getDirPath($logFileNameAndPath), $maxFiles, Logger::toMonologLevel($logLevel));

        WsHelper::sessionStart();
    }

}
