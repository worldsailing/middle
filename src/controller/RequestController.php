<?php
/**
 * Copyright (C) World Sailing, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Tamas Feiszt <tamas.feiszt@sailing.org>, 2017
 */

namespace Controller;

use Core\RestProcessInterface;
use worldsailing\Api\core\WsApiException;
use worldsailing\Helper\FileHelper;
use worldsailing\Helper\WsHelper;
use WsApp;
use Core\Response;
use Core\WsException;
use Core\RequestValidation;
use Service\CacheService;

/**
 * Class RequestController
 * @package Controller
 */
class RequestController extends AbstractController
{

    /**
     * @var array
     */
    private $parameters = [];

    /**
     * RequestController constructor.
     * @param WsApp $app
     * @throws WsException
     */
    public function __construct(WsApp $app)
    {
        parent::__construct($app);

        /**
         * Load cache service for tokens
         */
        if (! $this->app->cache()->isRegistered('token_cache')) {
            $driverName = $this->app->config()->get('common', 'tokenCacheDriver');
            if (CacheService::isValidDriver($driverName)) {
                $this->app->cache()->registerCache($driverName, 'token_cache');
            } else {
                throw new WsException('Invalid driver class in configuration. [' . $driverName . ']');
            }
        }

        /**
         * Load cache driver for responses
         */
        if (! $this->app->cache()->isRegistered('data_cache')) {
            $driverName = $this->app->config()->get('common', 'dataCacheDriver');
            if (CacheService::isValidDriver($driverName)) {
                $this->app->cache()->registerCache($driverName, 'data_cache');
            } else {
                throw new WsException('Invalid driver class in configuration. [' . $driverName . ']');
            }
        }

    }

    /**
     * @param $id
     * @param array $params
     */
    public function action($id, $params = [])
    {
        $this->app->log()->debug('Request action',  ['id' => $id, 'params' => $params]);
        $success = true;
        $data = null;

        // Collect request parameters - it overrides predefined parameters
        $this->mergeParams($this->app->input()->fetch());

        // Collect PHP parameters - it overrides any other predefined and collected parameters
        $this->mergeParams($params);

        //load request config
        $configKey = 'request' . WsApp::getInstance()->getDelimiter() . $id;
        $this->app->config()->load($configKey, 'request');

        //method
        $method = strtolower(WsHelper::getHttpMethod());

        //collect configuration of request using current method
        $requestConfig = $this->getRequestConfigByMethod($method);

        // Create validation process
        $validationProcess = $this->createRequestValidationById($id);

        // CSRF validation
        if ($validationProcess->validate($requestConfig)) {

            // Create and Run REST process
            /** @var RestProcessInterface $restProcess */
            $restProcess = $this->createRestProcess($requestConfig);

            try {
                $restResult = $restProcess->run($this->app, $this->parameters);

                $data = json_decode($restResult->getBody(), true);

                // Check REST result
                if (! $restResult->isSuccess()) {
                    $success = false;
                }

                // Send response
                (new Response($success, $data, $restResult->message(), $restResult->code(), $restResult->cached()))->dump();

            } catch (\Exception $e) {
                (new Response($success, null, $e->getMessage(), $e->getCode()))->dump();
            }

        } else {
            // Response with 403
            $this->app->log()->debug('Request not allowed [' . $validationProcess->validationError() . ']');
            (new Response($success, ['error' => 'Request not allowed'],WsHelper::getResponseMessageByCode(403), 403, false))->dump();
        }
    }


    /**
     * @param $id
     * @return RequestValidation
     */
    private function createRequestValidationById($id)
    {
        $validation = new RequestValidation($this->app, $id);
        return $validation;
    }

    /**
     * @param $method
     * @return mixed
     * @throws \Core\WsException
     */
    private function getRequestConfigByMethod($method)
    {
        $registeredMethods = $this->app->config()->get('request', 'methods');
        if ($registeredMethods && is_array($registeredMethods)) {
            foreach ($registeredMethods as $name => $registeredMethod) {
                if ($name == $method) {
                    return $registeredMethod;
                    break;
                }
            }
        }
        throw new WsException('Unregistered method [' . $method . ']');
    }


    /**
     * @param array $requestConfig
     * @return RestProcessInterface
     * @throws WsException
     */
    private function createRestProcess($requestConfig)
    {

        if (isset($requestConfig)) {
            if (isset($requestConfig['process'])) {
                $restClassName = '\\Process\\' . str_replace('/', '\\' ,$requestConfig['process']);
                $restClassFileName = FileHelper::setPathSlash(WS_PATH_SRC) . 'process/' . str_replace('\\','/',$requestConfig['process']) . '.php';

                if (is_file($restClassFileName)) {
                    require_once $restClassFileName;
                } else {
                    throw new WsException('Missing process file. [' . $restClassFileName . ']');
                }
                try {
                    /** @var RestProcessInterface $rest */
                    $rest = new $restClassName();

                    if (!$rest instanceof RestProcessInterface) {
                        throw new WsException('Incorrect process class. [' . $requestConfig['process'] . '] is not an instance of RestProcessInterface');
                    }

                    return $rest;
                } catch (\Exception $e) {
                    $this->app->log()->error('Registered process class does not exists', WsHelper::getExceptionContext());
                    throw new WsException($e->getMessage());
                }
            } else {
                throw new WsException('Missing required element in request config. [process]');
            }
        } else {
            throw new WsException('Missing process configuration.');
        }
    }

    /**
     * @param array $params
     * @return array
     */
    private function mergeParams($params = [])
    {
        if ($params && is_array($params)) {
            foreach ($params as $key => $param) {
                $this->parameters[$key] = $param;
            }
        }
        return $this->parameters;
    }


}
