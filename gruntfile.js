module.exports = function (grunt) {

    var package = grunt.file.readJSON('package.json');

    grunt.initConfig({
        /**
         * Scripts
         */

        //Build native js
        babel: {
            options: {
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            jsx: {
                files: [{
                    expand: true,
                    cwd: 'web/webapp/js/src/core/',
                    src: ['{**/*,*}.jsx'],
                    dest: 'web/webapp/js/build/core/',
                    ext: '.js'
                },{
                    expand: true,
                    cwd: 'web/webapp/js/src/components/',
                    src: ['{**/*,*}.jsx'],
                    dest: 'web/webapp/js/build/components/',
                    ext: '.js'
                },{
                    expand: true,
                    cwd: 'web/webapp/js/src/widgets/example/',
                    src: ['{**/*,*}.jsx'],
                    dest: 'web/webapp/js/build/widgets/example/',
                    ext: '.js'
                }]
            }
        },


        //Concat js files
        concat: {
            exampleGrid: {
                options: {
                    separator: ''
                },
                /* Watch out: INHERITANCE ORDER */
                src: ["web/webapp/js/build/core/*.js",
                    "web/webapp/js/build/components/Icon.js",
                    "web/webapp/js/build/components/OrderIcon.js",
                    "web/webapp/js/build/components/Link.js",
                    "web/webapp/js/build/components/Button.js",
                    "web/webapp/js/build/components/PaginationButton.js",
                    "web/webapp/js/build/components/ClickableColumnHeader.js",
                    "web/webapp/js/build/components/AbstractGridRow.js",
                    "web/webapp/js/build/components/AbstractGrid.js",
                    "web/webapp/js/build/widgets/example/grid/gridRow.js",
                    "web/webapp/js/build/widgets/example/grid/grid.js",
                    "web/webapp/js/build/widgets/example/grid/index.js"
                ],
                dest: 'web/webapp/js/concat/example/exampleGridWidget.js'
            },
            exampleForm: {
                options: {
                    separator: ''
                },
                /* Watch out: INHERITANCE ORDER */
                src: ["web/webapp/js/build/core/*.js",
                    "web/webapp/js/build/components/Icon.js",
                    "web/webapp/js/build/components/Link.js",
                    "web/webapp/js/build/components/Button.js",
                    "web/webapp/js/build/components/AbstractForm.js",
                    "web/webapp/js/build/widgets/example/exampleEntity.js",
                    "web/webapp/js/build/widgets/example/form/form.js",
                    "web/webapp/js/build/widgets/example/form/index.js"
                ],
                dest: 'web/webapp/js/concat/example/exampleFormWidget.js'
            }
        },


        // Minify JS
        uglify: {
            main: {
                options: {
                    beautify: package.devMode
                },
                files: [
                    {
                        expand: true,
                        cwd: 'web/webapp/js/concat/',
                        src: '{**/*,*}.js',
                        dest: 'web/webapp/js/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.js', '.min.js');
                        }
                    }
                ]
            }
        },


        /**
         * Styles
         */

        // Compile SCSS into CSS
        sass: {
            main: {
                files: {
                    'web/webapp/css/build/main.css': 'web/webapp/css/sass/main.scss'
                }
            }
        },

        // Clean map files
        clean: {
            scss: [
                'web/webapp/css/build/main.css.map'
            ]
        },

        // Minify CSS
        cssmin: {
            main: {
                options: {
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                files: [
                    {
                        expand: true,
                        cwd: 'web/webapp/css/build/',
                        src: '*.css',
                        dest: 'web/webapp/css/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.css', '.min.css');
                        }
                    }
                ]
            }
        },

        // No minify on css file if it's dev environment
        copy: {
            main: {
                src: 'web/webapp/css/build/main.css',
                dest: 'web/webapp/css/dist/main.min.css'
            },
        },


        /**
         * Watch
         */

        // watch changes
        watch: {
            js: {
                files: [
                    'web/webapp/js/src/core/*.jsx',
                    'web/webapp/js/src/core/{**/*,*}*.jsx',
                    'web/webapp/js/src/components/*.jsx',
                    'web/webapp/js/src/components/{**/*,*}*.jsx',
                    'web/webapp/js/src/widgets/{**/*,*}*.jsx'
                ],
                tasks: ['babel', 'concat', 'uglify']
            },
            css: {
                files: [
                    'web/webapp/css/sass/*.scss'
                ],
                tasks: ['sass:main', ((package.devMode === true) ? 'copy:main' : 'cssmin:main'), 'clean']
            }
        }

    });


    //register script operations
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    //register stylesheet operations
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    //register watching
    grunt.loadNpmTasks('grunt-contrib-watch');


    //start
    if (package.devMode === true) {
        grunt.registerTask('default', ['babel', 'concat', 'uglify', 'sass', 'copy', 'clean', 'watch']);
    } else {
        grunt.registerTask('default', ['babel', 'concat', 'uglify', 'sass', 'cssmin', 'clean']);
    }
};